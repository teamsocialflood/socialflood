<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		sf-core-back-end-function.php
//	Path:		core/front-end/lib/php/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Front-End Functions - Functions that are used only on the front end - Loaded in the front-end
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Require SocialFlood Front-End Core Header
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_core_front_end_header() {
		require_once(FRONT_END_CORE.'header.php');
	}
/***************************************************************************************************************************/
//	Require SocialFlood Front-End Core Footer
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_core_front_end_footer() {
		require_once(FRONT_END_CORE.'footer.php');
	}
/***************************************************************************************************************************/
//	Functions that check whether or not a given username is valid and available.
//	Since 0.0.1
//	Param string $username Username.
//	Return boolean True on success, and False on failure.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Function for checking if a username is available	
 	function sf_username_taken( $username ) {
		$sfdb = sfdb();
		$dbq = $sfdb->prepare( "SELECT * FROM sf_user_meta WHERE meta_key='username' AND meta_content = '".$username."'" );
		$dbq->execute();
		$row_count = $dbq->rowCount();
		//echo $row_count;
		if ( $row_count != 0 ) {
			return true;
		} elseif ( $row_count == 0 ) {
			return false;
		}
	}

	//	Function for checking a username is valid
	function sf_check_username( $username = '' ) {
		/*$response = array(
				'ok'	=> true, 
				'msg'	=> "Axax is working.");
		
		return $response;*/
		
		//$username = trim($username); // strip any white space
		$response = array(); // our response
		// if the username is blank
		if ( $username =='' ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Empty");
		// if the username does not match a-z or '.', '-', '_' then it's not valid
		} else if ( !preg_match( '/^[a-z0-9.-_]+$/', $username ) ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Only use a-Z, 0-9, or .-_");
		// this would live in an external library just to check if the username is taken
		} else if ( sf_username_taken( $username ) ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Taken");
		// it's all good
		} else {
			$response = array(
				'ok'	=> true, 
				'msg'	=> "Available");
		}
		return $response;
	}
		// Trigger username validation - NON-AJAX
		if ( isset( $_POST['check_username'] ) && $_POST['ajax'] == 'no'  ) {
			global $response_array;
			$response_array = sf_check_username( $_POST['check_username'] );
			
		}
/***************************************************************************************************************************/
//	Functions that check whether the given email is valid and unused.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper Function that checks if the given emeil is a valid email address string.
	// Since 0.0.1
	// Param string $email Email.
	// Return boolean True on success, and False on failure.
	function sf_is_email( $email ) {
		if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
			return true;
		}
		else {
			return false;
		}
	}

	// Helper Function that checks whether the given email is being used.
	// Since 0.0.1
	// Param string $username Username.
	// Return boolean True on success, and False on failure.
	function sf_email_exists( $email ) {
		$sfdb = sfdb();
		$dbq = $sfdb->prepare( "SELECT * FROM sf_users WHERE email = '".$email."'" );
		$dbq->execute();
		//$dbq->fetch( PDO::FETCH_ASSOC );
		$row_count = $dbq->rowCount();
		if ( $row_count != 0 ) {
			return true;
		} elseif ( $row_count == 0 ) {
			return false;
		}
	}

	// Root Function for checking an email is valid and unused
	// Since 0.0.1
	// Param string $username Username.
	// Return boolean True on success, and False on failure.
	function sf_check_email( $email ) {
		if ( !sf_is_email( $email ) ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> 'Invalid email address');
		} else if ( sf_is_email( $email ) ) {
			if ( sf_email_exists( $email ) ) {
				$response = array(
					'ok'	=> false,
					'msg'	=> 'Only one account per email is allowed');
			} else if ( !sf_email_exists( $email ) ) {
				$response = array(
					'ok'	=> true,
					'msg'	=> 'Validated: '.$email );
			}
		}
		return $response;
	}

	//	Run email validation - NON-AJAX
	if ( isset( $_POST['check_email'] ) && $_POST['ajax'] == 'no' ) {
    	global $response_array;
		$response_array = sf_check_email( $_POST['check_email'] );
    	
	}
/***************************************************************************************************************************/
//	Functions that register the user and send an email verification link
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_send_email_verification_link( $email, $link_code ) {
		$verification_link = "<a href='".BASE_URL."verify/?email=".$email."&verification_code=".$link_code."'>Verify Me!</a>";
		// Email the user a verification link
		$headers	=	'From: '.SITENAME.' Activations support@'.NAKED_SITE_URL."\r\n";
		$headers	.=	'Reply-To: no-reply@'.NAKED_SITE_URL."\r\n";
		$headers	.=	"Content-type: text/html\r\n";
		$subject	=	'Your New '.SITENAME.' Account - Please verify your email address.';
		$message	=	"<p><a href='".BASE_URL."'><img src='".BASE_URL."assets/logo/png/".SMALL_SITE_LOGO."'></a></p>".
						"<p>Hello,</p>".													// Activation email message
						"<p>To verify your email address please click the link below.</p>".
						"<p>$verification_link</p>".										// Verification link
						"<p>Thank you,<br>".												// Email closing
						SITENAME.' Support Team'."</p>";										// Signature
		mail( $email, $subject, $message, $headers );										// Send the verification email
	}

	function sf_register_user($post) {
		$sfdb = sfdb();
		$post = (object) $post;
		if ( $post->username != '' )			{ $un = true; }		else { $un = false; }
		if ( $post->email != '' ) 				{ $em = true; }		else { $em = false; }
		if ( $post->email2 != '' ) 				{ $em2 = true; }	else { $em2 = false; }
		if ( $post->email == $post->email2 ) 	{ $emm = true; }	else { $emm = false; }
		if ( strlen( $post->password ) > 7 ) 	{ $pw = true; }		else { $pw = false; }
		if ( $post->fname != '' )				{ $fn = true; }		else { $fn = false; }
		if ( $post->lname != '' )				{ $ln = true; }		else { $ln = false; }
		if ( $post->byear != '' )				{ $by = true; }		else { $by = false; }
		if ( $post->bmonth != '' )				{ $bm = true; }		else { $bm= false; }
		if ( $post->bday != '' ) 				{ $bd = true; }		else { $bd = false; }
		if ( $post->gender != '' )				{ $gd = true; }		else { $gd = false; }
		if ( $post->ajax == 'yes' )				{ $aj = true; }		else { $aj = false; }
		
		$email =		$post->email;
		$password = 	$post->password;
		
		$user_array = array (
			':email'		=> pg_escape_string( $email ),
			':password'		=> sha1( md5( pg_escape_string( $password ) ) ),
			);

		$link_code = md5( sha1( $email.$post->username.$password ) );
		
		$user_meta_array = array(
			'username'			=> $post->username,
			'domain'			=> BASE_URL.$post->username,
			'fname'				=> $post->fname,
			'lname'				=> $post->lname,
			'bday'				=> $post->bday,
			'bmonth'			=> $post->bmonth,
			'byear'				=> $post->byear,
			'gender'			=> $post->gender,
			'activated' 		=> '0',
			'activation_code'	=> $link_code );
		
		if ($aj) {
			$i = $sfdb->add_user( $user_array, $user_meta_array, 1 );			
			if ( $i['ok'] )	{
				$response = array(
					'ok'		=>	true,
					'msg'		=>	'Thank you for signing up at Lurkable,'.$post->fname.'.<br>'.
									'An email verification link has been sent to '.
									$email.'.<br>'.'Please check your email to complete the signup.' );
				sf_send_email_verification_link( $email , $link_code );
			} elseif ( !$i['ok'] ) {
				raise_error( $i['msg'] );
				$response = array(
					'ok'		=>	false,
					'msg'		=>	"An internal error has occured. It's not your fault. The error was reported!<br>" );
			}
		} elseif ( !$aj ) {
			if ($un && $em && $em2 && $emm && $pw && $fn && $ln && $by && $bm && $bd && $gd) {
				$i = $sfdb->add_user( $user_array, $user_meta_array, 0 );
				if ( $i['ok'] ) {
					$response = array(
						'ok'		=>	true,
						'msg'		=>	'Thank you for signing up at Lurkable.<br>'.
										'An email verification link has been sent to '.
										$post->email.'.<br>'.'Please check your email to complete the signup.' );
					sf_send_email_verification_link( $email , $link_code );
				} elseif ( !$i['ok'] ) {
					raise_error( $i['msg'] );
					$response = array(
						'ok'		=>	false,
						'msg'		=>	"An internal error has occured. It's not your fault. The error was reported!<br>" );
				}
			} elseif ( !$un ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please enter your desired username.<br>' );
					return $response;
			} elseif ( !$em ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your email.<br>' );
					return $response;
			} elseif ( !$em2 ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must re-enter your email.<br>' );
					return $response;
			} elseif ( !$emm ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'The email fields must match exactly.<br>' );
					return $response;
			} elseif ( !$pw ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Password must be 8+ characters using a-Z, 0-9, [.-_].<br>' );
					return $response;
			} elseif ( !$fn ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please provide your real first name.<br>' );
					return $response;
			} elseif ( !$ln ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please provide your real last name.<br>' );
					return $response;
			} elseif ( !$by ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth year.<br>' );
					return $response;
			} elseif ( !$bm ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth month.<br>' );
					return $response;
			} elseif ( !$bd ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth day.<br>' );
					return $response;
			} elseif ( !$gd ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please select your gender.<br>' );
					return $response;
			}
		}
		return $response;	
	}

	//	Run the registration functions - NON-AJAX
	if ( $_POST['regsubmit'] && $_POST['ajax'] == 'no' ) {
		global $response_array;
		$response_array = sf_register_user($_POST);
		$response_array = (object) $response_array;
	}
/***************************************************************************************************************************/
//	Login functions & triggers
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Try to log the user in
	function sf_try_login( $post ) {
		echo "trying login";
		$stay_logged_in = $post['rememberme'];
		if ( !sf_is_user_logged_in() && strlen($post['log']) > 0 && strlen($post['pwd']) > 0 ) {
			$sfdb = sfdb();
			$authenticated = $sfdb->authenticate_user( $post['log'], $post['pwd'] );
			if ( $authenticated['ok'] ) {
				global $loggedin_user;
				$loggedin_user = $authenticated['user'];
				$loggedin_user = (object) $loggedin_user;
				define( 'CURRENT_USER_ID', $loggedin_user->id );
				$login_token = gen_rand_str( 77 );
				$url = NAKED_SITE_URL;
				//echo $url;
				$_SESSION['logged_in'] = true;
				$_SESSION['uid'] = $loggedin_user->id;
				if ( $stay_logged_in == 'forever' ) {
					$_SESSION['logout_token'] = $login_token;
					$result = $sfdb->set_login_token( $loggedin_user->id, $login_token );
					//log_event( $result['msg'] );
					$cookie = $loggedin_user->id . ':' . $login_token;
					$mac = hash_hmac('sha256', $cookie, SECURE_AUTH_KEY);
					$cookie .= ':' . $mac;
					setcookie(SITENAME.'_rememberme', $cookie, time() + (2 * 365 * 24 * 60 * 60), "/", $url, true, true);
				} else {
					setcookie(SITENAME.'_rememberme', 'void', time()-3600, "/", $url, true, true);
				}
				header('Location: https://'.NAKED_SITE_URL, true, 302);
			}  else {
				global $response_array;
				$response_array = array(
					'ok'	=> false, 
					'msg'	=> "The login information provided was incorrect!");
			}
		}
	}
		// Login trigger
		if ( $_POST['try_login'] ) {
			sf_try_login( $_POST );
			//print_r( $_POST );
		}
/***************************************************************************************************************************/
/***************************************************************************************************************************/





//	Blank
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/***************************************************************************************************************************/

//	FOR DEBUGGING ONLY!
//	Comment out this entire section before uploading to production server.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
/***************************************************************************************************************************/
?>