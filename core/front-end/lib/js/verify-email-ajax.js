<!--
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*	Package:		SocialFlood Social Engine
/*	File:			verify-email-ajax.js - /engine/javascript/
/*	Version:		0.0.1
/*	Revisions:		0
/*	Last Update:	1.20.2013
/*	Description:	This is the SocialFlood email verification ajax
/***************************************************************************************************************************/

//	BEGIN EMAIL VERIFICATION FUNCTION
	$(document).ready(function() {
		BASE_URL = window.location.origin;
		$('#verifybutton').hide(); // Hide the verify button for people with JavaScript enabled

		var user = $('#user').val();
		var code = $('#code').val();
		var extra = $('#extra').val();
		
		// show our holding text in the validation message space
		verifyM =	'<img src="' + BASE_URL + '/assets/gif/ajax-loader.gif" height="16" width="16"> ' +
					'Attempting to activate your account...';
		$('#verifyMessage').html(verifyM);
		
		$data = encodeURIComponent(user) + '&code=' + encodeURIComponent(code) + '&extra=' + encodeURIComponent(extra);
		
		$.ajax({
			url: BASE_URL + '/includes/functions.php',
			data: 'ajax=yes&verify=true&user=' + $data,
			dataType: 'json',
			type: 'post',
			success: function (j) {
				//alert('Success!');
				if (j.ok) {
					//alert(j.msg);
					$('#verifyMessage').html(j.msg);
					// initiate a timeout
					setTimeout(function() {
						var url = BASE_URL;
					//	redirect to user stream
						$(location).attr('href',url);
					}, 12000); // set delay 5 seconds
				} else {
					//alert(j.msg);
					$('#verifyMessage').html(j.msg);
				}
			}
		});
	
	});
//	END EMAIL VERIFICATION FUNCTION
-->