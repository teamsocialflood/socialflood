<!--
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*	Package:		SocialFlood Social Networking Engine
/*	File:			reg-form-ajax.js - /js/
/*	Version:		0.0.3
/*	Revisions:		3
/*	Last Update:	1.20.2013
/*	Description:	This is the SocialFlood registration form ajax
/***************************************************************************************************************************/
-->
	
	

<!-- BEGIN JAVASCRIPT/AJAX FORM VALIDATION SCRIPT -->
	<!--
	$(document).ready(function () {
		BASE_URL = window.location.origin + "/";
		// set this to the base url for the site
		var api_key = $('#api_token').val();
		$('#registerbutton').attr('type','button'); // Change functionality of the button for people with JavaScript enabled
		$('#ajax').attr('value','yes');
		
		var un_validated = false;
		var em_validated = false;
		var em2_validated = false;
		
		var un_att;
		var em_att;
		var em2_att;
		
		var validateUsername = $('#validateUsername');
		var validateEmail = $('#validateEmail');
		
		span1 = 'Empty';
		span2 = 'Empty';
		
		$("#registerbutton").attr("disabled", true);
		
		validateUsername.html(span1);
		
		validateEmail.html(span2);
				
		$('[name="log"]').focus();
		
		function field_check() {
			this.timer = setTimeout(function () {
				// set the username boolean from the 'ok' field of the $response array
				un = $("#username").attr("class");
				
				// set the email boolean from the 'ok' field of the $response array
				em = $("#email").attr("class");
				
				// set the email boolean from the 'ok' field of the $response array
				em2 = $("#email2").attr("class");
				
				g = $("input[name='gender']:checked").val();
					
				$("input[name='gender']:checked").val()
				// compare the two $response booleans and validate the form and button...
				if ( g == 'male' || g == 'female' && un == 'valid' &&  em == 'valid' && em2 == 'valid' && $('#fname').val() && $('#lname').val() && $('#password').val().length >7 ) {
					$("#registerbutton").removeAttr("disabled");
					$("#registerbutton").attr('onclick','register()');
				} else {	// or invalidate the form and button
					$("#registerbutton").attr("disabled", true);
					$("#registerbutton").removeAttr("onclick");
				}
			}, 1000);			
		}
				
		// Username validation logic function
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function un_field_validate(t) {
			
			// only run the check if the username has actually changed - also means we skip meta keys
			if (t.value != t.lastValue) {
				
				// the timeout logic means the ajax doesn't fire with *every* key press, i.e. if the user holds down
				// a particular key, it will only fire when the release the key.
								
				if (t.timer) clearTimeout(t.timer);
				
				// show our holding text in the validation message space
				span1 = '<img src="' + BASE_URL + 'assets/gif/ajax-loader.gif" height="16" width="16"> Checking...';
				validateUsername.removeClass('error').html(span1);
				
				// fire an ajax request for the validation result
				t.timer = setTimeout(function () {
					url = BASE_URL + 'api/local/';
					$.ajax({
						url: url,
						data: 'api_token=' + api_key + '&ajax=yes&check_username=' + t.value,
						dataType: 'json',
						type: 'post',
						success: function (j) {
							// check the 'ok' field from the $response array
							if (j.ok) {
								$("#username").attr('class','valid'); un_validated = true;
							} else {
								$("#username").attr('class','invalid'); un_validated = false;
							}
							
							field_check();
							
							//	put the 'msg' field from the $resp array from check_username (php code)
							//	in to the validation message
							validateUsername.html(j.msg);
						}
					});
				}, 200);
				
				// copy the latest value to avoid sending requests when we don't need to
				t.lastValue = t.value;
			}	
		}
		/*************************************************************************************************************/
		
		// Email validation logic function
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function em_field_validate(t2) {
			
			// only run the check if the email has actually changed - also means we skip meta keys
			if (t2.value != t2.lastValue) {
				
				// the timeout logic means the ajax doesn't fire with *every* key press, i.e. if the user holds down
				// a particular key, it will only fire when the release the key.
								
				if (t2.timer) clearTimeout(t2.timer);
				
				// show our holding text in the validation message space
				span2 = '<img src="' + BASE_URL + 'assets/gif/ajax-loader.gif" height="16" width="16"> Checking...';
				validateEmail.removeClass('error').html(span2);
				
				// fire an ajax request in 1/5 of a second
				t2.timer = setTimeout(function () {
					$.ajax({
						url: BASE_URL + 'api/local/',
						data: 'api_token=' + api_key + '&ajax=yes&check_email=' + t2.value,
						dataType: 'json',
						type: 'post',
						success: function (j) {
							
							// check the 'ok' field from the $response array
							if (j.ok) {
								$("#email").attr('class','valid'); em_validated = true;
							} else {
								$("#email").attr('class','invalid'); em_validated = false;
							}
							
							field_check();
							
							// put the 'msg' field from the $response array from check_email (php code)
							// in to the validation message
							validateEmail.html(j.msg);
						}
					});
				}, 200);
				
				// copy the latest value to avoid sending requests when we don't need to
				t2.lastValue = t2.value;
			}
		}
		/*************************************************************************************************************/
		
		$('#fname').keyup(function () {
			if ($('#fname').val().length > 0 ) {
				$("#fname").attr('class','valid'); em2_validated = true;
			} else {
				$("#fname").attr('class','invalid'); em2_validated = false;
			}
			
			field_check();
			
		});
		
		$('#lname').keyup(function () {
			if ($('#lname').val().length > 0 ) {
				$("#lname").attr('class','valid'); em2_validated = true;
			} else {
				$("#lname").attr('class','invalid'); em2_validated = false;
			}
			
			field_check();
			
		});
		
		//	Field event handeling for username change focus, keyup and mouseup
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('#username').blur(function () {			// Keyup
			// cache the 'this' instance as we need access to it within a setTimeout, where 'this' is set to 'window'
			var t = this;
			if ($('#username').val().length != 0) {
				un_field_validate(t);
			} else {
				validateUsername.html('Empty');
				$("#username").attr('class','invalid'); un_validated = false;
				field_check();
			}
		});
		/*************************************************************************************************************/

		//	Field event handeling for email change focus, keyup and mouseup
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('#email').blur(function () {				// Keyup
			// cache the 'this' instance as we need access to it within a setTimeout, where 'this' is set to 'window'
			var t2 = this;
			if ($('#email').val().length != 0) {
				em_field_validate(t2);
			} else {
				validateEmail.html('Empty');
				$("#email").attr('class','invalid'); un_validated = false;
				field_check();
			}
			
		});
		/*************************************************************************************************************/
		
		//	Field event handeling for email change focus, keyup and mouseup
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('#email2').blur(function () {				// Keyup
			// cache the 'this' instance as we need access to it within a setTimeout, where 'this' is set to 'window'
			if ($('#email2').val() == $('#email').val() && $('#email2').val().length != 0) {
				$("#email2").attr('class','valid'); em2_validated = true;
			} else {
				$("#email2").attr('class','invalid'); em2_validated = false;
			}
			
			field_check();

		});
		/*************************************************************************************************************/
		
		$('#password').keyup(function () {
		if ($('#password').val().length > 7 ) {
				$("#password").attr('class','valid'); em2_validated = true;
			} 
			field_check();
		});
		
		$('#password').blur(function () {
			if ($('#password').val().length > 7 ) {
				$("#password").attr('class','valid'); em2_validated = true;
			} 
			else {
				$("#password").attr('class','invalid'); em2_validated = false;
			}
			field_check();
		});
		
		
		$('#byear').mouseup(function () {
			field_check();
		});
		
		$('#bmonth').mouseup(function () {
			field_check();
		});
		
		$('#bday').mouseup(function () {
			field_check();
		});
		
		$('#f-gender').mouseup(function () {
			field_check();
		});
		
		$('#m-gender').mouseup(function () {
			field_check();
		});
		
		$('#fgender').mouseup(function () {
			field_check();
		});
		
		$('#mgender').mouseup(function () {
			field_check();
		});
		
		
	});
	-->
<!-- END JAVASCRIPT/AJAX FORM VALIDATION SCRIPT -->

<!-- BEGIN REGISTER FUNCTION -->
	<!--
	function register() {
		var username = $('#username').val();
		var email = $('#email').val();
		
		// show our holding text in the validation message space
		successM =	'<img src="' + BASE_URL + 'assets/gif/ajax-loader.gif" height="16" width="16"> ' +
					'Contacting the server...';
		$('#responseMessage').html(successM);
		
		data = $("#reg_form").serialize();
		
		$.ajax({
			url: BASE_URL + 'api/local/',
			data: data,
			dataType: 'json',
			type: 'post',
			success: function (j) {
				if (j.ok) {
					$('#reg_form').slideUp(500);
					$('#responseMessage').html(j.msg);
				} else {
					$('#reg_form').slideUp(500);
					$('#responseMessage').html(j.msg);
				}
			}
		});
	}
	-->
<!-- END REGISTER FUNCTION -->