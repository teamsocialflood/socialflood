<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		sf-core-back-end-function.php
//	Path:		core/back-end/lib/php/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Back-End Functions - Functions that are used only on the back end - Loaded in the back-end
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Require SocialFlood Back-End Core Header
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_core_back_end_header() {
		require_once( BACK_END_CORE.'header.php' );
	}
/***************************************************************************************************************************/
//	Require SocialFlood Back-End Core Footer
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_core_back_end_footer() {
		require_once( BACK_END_CORE.'footer.php' );
	}
/***************************************************************************************************************************/
//	Activate a useres login account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function activate_user( $get ) {
		$get = (object) $get;
		$email = $get->email;
		$code = $get->activation_code;
		$sfdb = sfdb();
		$i = $sfdb->activate_account( $email, $code );
		if ( $i['ok'] ) {
			
			// Email the user for verification
			$headers	=	'From: '.SITENAME.' Activations support@'.NAKED_SITE_URL."\r\n";
			$headers	.=	'Reply-To: no-reply@'.NAKED_SITE_URL."\r\n";
			$headers	.=	"Content-type: text/html\r\n";
			$subject	=	'Your New '.SITENAME.' Account - Your account has been activated.';
			$message	=	"<p>Hello,</p>".													// Activation email message
							"<p>Your email has been verified and your login has been activated.</p>".
							"<p>You can login and setup your account using these credentials.</p>".
							"<p>Username: ".$user."<br>".										// The login username
							'Password: '.$password."</p>".										// The user's password
							"<p>".$login_link."</p>".
							"<p>You can always change your password under account settings.</p>".
							"<p>".$change_pass_link."</p>".
							"<p>Happy lurking,<br>".											// Email closing
							SITENAME.' Support Team'."</p>";										// Signature
			mail( $email, $subject, $message, $headers );										// Send the verification email
			$creds = array(
				'user_login'	=> $user,
				'user_password'	=> $password,
				'remember'		=> true );
			if ( $rid == $uid) { $user_logged = sf_signon( $creds, true ); }
			if ( !$user_logged['ok'] ) { $login_error = $user_logged->get_error_message();
				$errorM	=	'There was an error automatically logging in the user, '.
							$user.', while completing the account activation process. - Error: '.
							$login_error;
				raise_error ($errorM);
				$response	= array (
					'ok'	=> false,
					'msg'	=> 'There was an error automatically logging you in.' );
			} else {
				$response	=	array (
					'ok'	=>	true,
					'msg'	=>	'Your email has been verified.'."<br>".
								'You may now log in.'."<br>" );
			}
		} elseif ( !$i['ok'] ) {
			$response	= array (
				'ok'	=> false,
				'msg'	=> $i['msg'] );
		}
		return $response;
	}

//	Run account activation function - NON-AJAX
	if ( $_GET['activation_code'] && $_GET['email'] ) {
		global $response_array;
		 activate_user( $_GET );
		//$response_array = (object) $response_array;
	}
/***************************************************************************************************************************/
//	Function that produces the user toolbox
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function do_user_toolbox() {
		require_once( CONTROLERS.'user-toolbox.php' );
	}
/***************************************************************************************************************************/
//	Returns the logged in user's domain
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_loggedin_user_domain() {
		if ( sf_user_logged_in() ) {
			global $loggedin_user;
			echo $loggedin_user->domain;
		} else {
			return false;
		}
	}
/***************************************************************************************************************************/
//	Returns the logged in user's avatar
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_loggedin_user_avatar( $w, $h ) {
		if ( sf_is_user_logged_in() ) {
			global $loggedin_user;
			$url = $loggedin_user->avatar_url;
			return '<img class="stream-avatar loggedin-avatar" id="whats-new-loggedin-avatar" width="'.$w.'" height="'.$h.'" src="'.$url.'"/><br>';
		} else {
			return false;
		}
	}
/***************************************************************************************************************************/
// User logout functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Echo the logout url
	function logout_link() {
		if ( isset( $_SESSION['logout_token'] ) ) {
			echo BASE_URL."?logout=".$_SESSION['logout_token'];
		} else {
			echo BASE_URL."?logout=yes";
		}
	}
	// Perform logout for a user
	function logout_user( $token ) {
		if ( sf_user_logged_in() ) {
			$uid = $_SESSION['uid'];
			if ( isset( $_SESSION['logout_token'] ) ) {
				$sfdb = sfdb();
				$response = $sfdb->logout_user( $uid, $token );
				if ( $response[ok] ) {
					setcookie(SITENAME.'_rememberme', 'void', time()-3600, "/", $url, false, true);
					unset( $_SESSION['logged_in'] );
					unset( $_SESSION['uid'] );
					header("Location: ".BASE_URL);
				}
			}
			unset( $_SESSION['logged_in'] );
			unset( $_SESSION['uid'] );
			header("Location: ".BASE_URL);
		}
	}
		// Logout trigger
		if ( $_GET['logout'] ) {
			logout_user( $_GET['logout'] );
		}
/***************************************************************************************************************************/
/***************************************************************************************************************************/





//	Blank
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/***************************************************************************************************************************/

//	FOR DEBUGGING ONLY!
//	Comment out this entire section before uploading to production server.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
/***************************************************************************************************************************/
?>