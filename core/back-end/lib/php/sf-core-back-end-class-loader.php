<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		sf-core-back-end-class-loader.php
//	Path:		/core/back-end/lib/php/
//	Version:	0.0.1
//	Updated:	3/28/2013
/***************************************************************************************************************************/

//	Require all classes in the proper order.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	require_once( CORE_BACK_LIB_PHP_CLASSES.'sf-media-embed.clss' );
	require_once( CORE_BACK_LIB_PHP_CLASSES.'sf-user-comm.clss' );
	require_once( CORE_BACK_LIB_PHP_CLASSES.'sf-activity.clss' );
	require_once( CORE_BACK_LIB_PHP_CLASSES.'sf-interaction.clss' );
	require_once( CORE_BACK_LIB_PHP_CLASSES.'sf-advertisement.clss' );
/***************************************************************************************************************************/
?>