// JavaScript Document
<!--
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*	Package:		BTC Mutual Online Accounts Management Application
/*	File:			change-pass-ajax.js - /javascript/
/*	Version:		0.0.1
/*	Revisions:		0
/*	Last Update:	1.20.2013
/*	Description:	This is the BTC Mutual Account Manager password change ajax script
/***************************************************************************************************************************/

//	BEGIN EMAIL VERIFICATION FUNCTION

	function sp_generate() {
		setupM =	'<img src="http://btcmutual.com/assets/gif/ajax-loader.gif" height="16" width="16"> ' +
					'Communicating with the server...';
		$('#validateNewPass').html(setupM);	
		$.ajax({
			url: 'http://btcmutual.com/includes/functions.php',
			data: 'ajax=yes&sp_generate=true',
			dataType: 'json',
			type: 'post',
			success: function (j) {
				//console.log(j);
				if (j.ok) {
					//alert(j.msg);
					$('#validateNewPass').html(j.msg);
					setTimeout(function() {
						$('#new_pass1').val(j.spw).keyup();
					}, 2000); // set delay 5 seconds
					
					setTimeout(function() {
						$('#new_pass2').val(j.spw).keyup();
					}, 4000); // set delay 5 seconds
				} else {
					//alert(j.msg);
					$('#validateNewPass').html(j.msg);
				}
			}
		});
	}

	function change_password() {
		// show our holding text in the validation message space
		setupM =	'<img src="http://btcmutual.com/assets/gif/ajax-loader.gif" height="16" width="16"> ' +
					'Communicating with the server...';
		$('#changepassMessage').html(setupM);
		
		data = $("#change_pass_form").serialize();
		
		//alert(data);
		
		$.ajax({
			url: 'http://btcmutual.com/includes/functions.php',
			data: data,
			dataType: 'json',
			type: 'post',
			success: function (j) {
				//console.log(j);
				if (j.ok) {
					//alert(j.msg);
					$('#changepassMessage').html(j.msg);
					setTimeout(function() {
						var url = "http://btcmutual.com";
					//	redirect to change password page
						$(location).attr('href',url);
					}, 4000); // set delay 5 seconds
				} else {
					//alert(j.msg);
					$('#changepassMessage').html(j.msg);
				}
			}
		});
	}
	
	$(document).ready(function () {
		
		$('#ajax').attr('value','yes');
		
		$('#changepassbutton').attr('type','button'); // Hide the verify button for people with JavaScript enabled
		
		var op_verified		= false;
		var np_validated	= false;
		var np_match		= false;
		
		var verifyOldPass	= $('#verifyOldPass');
		var validateNewPass	= $('#validateNewPass');
		var validateMatch	= $('#validateMatch');
		
		span1 = '<p>Enter your existing password.</p>';
		span2 = '<p>Enter a new password.</p>';
		span3 = '<p>Confirm the new password.</p>';
		
		$("#changepassbutton").attr("disabled", true);
		
		$('#old_pass').addClass('invalid');
		verifyOldPass.html(span1);
		
		$('#new_pass1').addClass('invalid');
		validateNewPass.html(span2);
		
		$('#new_pass2').addClass('invalid');
		validateMatch.html(span3);
		
		// Verify Old Pass logic function
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function verify_oldpass(t) {
			
			// only run the check if the old_pass has actually changed - also means we skip meta keys
			if (t.value != t.lastValue) {
				
				// the timeout logic means the ajax doesn't fire with *every* key press, i.e. if the user holds down
				// a particular key, it will only fire when the release the key.
								
				if (t.timer) clearTimeout(t.timer);
				
				// show our holding text in the validation message space
				span1 =
					'<p><img src="http://btcmutual.com/assets/gif/ajax-loader.gif" height="16" width="16"> Checking...</p>';
				verifyOldPass.removeClass('error').html(span1);
				
				// fire an ajax request for the validation result
				t.timer = setTimeout(function () {
					$.ajax({
						url: 'http://btcmutual.com/includes/functions.php',
						data: 'ajax=yes&verify_pass=' + t.value,
						dataType: 'json',
						type: 'post',
						success: function (j) {
							
							// check the 'ok' field from the $response array
							if (j.ok) {
								$("#old_pass").attr('class','valid'); op_verified = true;
							} else {
								$("#old_pass").attr('class','invalid'); op_verified = false;
							}
							
							// set the old_pass boolean from the 'ok' field of the $response array
							op_att = $("#old_pass").attr("class");
							
							// set the new_pass1 boolean from the 'ok' field of the $response array
							np1_att = $("#new_pass1").attr("class");
							
							// set the new_pass1 boolean from the 'ok' field of the $response array
							np2_att = $("#new_pass2").attr("class");
							
							// compare the two $response booleans and validate the form and button...
							if ( op_att == 'valid' &&  np1_att == 'valid' && np2_att == 'valid' ) {
								$("#changepassbutton").removeAttr("disabled");
								$("#changepassbutton").attr('onclick','change_password()');
							} else {	// or invalidate the form and button
								$("#changepassbutton").attr("disabled", true);
								$("#changepassbutton").removeAttr("onclick");
							}
							
							//	put the 'msg' field from the $resp array from check_old_pass (php code)
							//	in to the validation message
							verifyOldPass.html(j.msg);
						}
					});
				}, 200);
				
				// copy the latest value to avoid sending requests when we don't need to
				t.lastValue = t.value;
			}	
		}
		/*************************************************************************************************************/
		
		// New Password validation - logic function
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function validate_newpass(t2) {
			
			// only run the check if the new_pass1 has actually changed - also means we skip meta keys
			if (t2.value != t2.lastValue) {
				
				// the timeout logic means the ajax doesn't fire with *every* key press, i.e. if the user holds down
				// a particular key, it will only fire when the release the key.
								
				if (t2.timer) clearTimeout(t2.timer);
				
				// show our holding text in the validation message space
				span2 =
					'<p><img src="http://btcmutual.com/assets/gif/ajax-loader.gif" height="16" width="16"> Checking...</p>';
				validateNewPass.removeClass('error').html(span2);
				
				// fire an ajax request in 1/5 of a second
				t2.timer = setTimeout(function () {
					$.ajax({
						url: 'http://btcmutual.com/includes/functions.php',
						data: 'ajax=yes&validate_pass=' + t2.value,
						dataType: 'json',
						type: 'post',
						success: function (j) {
							
							// check the 'ok' field from the $response array
							if (j.ok) {
								$("#new_pass1").attr('class','valid'); np_validated = true;
							} else {
								$("#new_pass1").attr('class','invalid'); np_validated = false;
							}
							
							// set the old_pass boolean from the 'ok' field of the $response array
							op_att = $("#old_pass").attr("class");
							
							// set the new_pass1 boolean from the 'ok' field of the $response array
							np1_att = $("#new_pass1").attr("class");
							
							// set the new_pass1 boolean from the 'ok' field of the $response array
							np2_att = $("#new_pass2").attr("class");
							
							// compare the two $response booleans and validate the form and button...
							if ( op_att == 'valid' &&  np1_att == 'valid' && np2_att == 'valid' ) {
								$("#changepassbutton").removeAttr("disabled");
								$("#changepassbutton").attr('onclick','change_password()');
							} else {	// or invalidate the form and button
								$("#changepassbutton").attr("disabled", true);
								$("#changepassbutton").removeAttr("onclick");
							}
							
							// put the 'msg' field from the $response array from check_new_pass1 (php code)
							// in to the validation message
							validateNewPass.html(j.msg);
						}
					});
				}, 200);
				
				// copy the latest value to avoid sending requests when we don't need to
				t2.lastValue = t2.value;
			}
		}
		/*************************************************************************************************************/

		// Check new pass and confirm pass fields match - logic function
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function check_newpass(t3) {
			
			// only run the check if the new_pass1 has actually changed - also means we skip meta keys
			if (t3.value != t3.lastValue) {
				
				// the timeout logic means the ajax doesn't fire with *every* key press, i.e. if the user holds down
				// a particular key, it will only fire when the release the key.
								
				if (t3.timer) clearTimeout(t3.timer);
				
				// show our holding text in the validation message space
				span2 =
					'<p><img src="http://btcmutual.com/assets/gif/ajax-loader.gif" height="16" width="16"> Checking...</p>';
				validateMatch.removeClass('error').html(span2);
				
				// fire an ajax request in 1/5 of a second
				t3.timer = setTimeout(function () {
					np1 = $('#new_pass1').val();
					np2 = $('#new_pass2').val();
					// check the 'ok' field from the $response array
					if (np1 == np2) {
						$("#new_pass2").attr('class','valid'); np_match = true;
						msg = '<p>Confirmed!</p>';
					} else {
						$("#new_pass2").attr('class','invalid'); np_match = false;
						msg = '<p>Not Matching!</p>';
					}
					
					// set the old_pass boolean from the 'ok' field of the $response array
					op_att = $("#old_pass").attr("class");
					
					// set the new_pass1 boolean from the 'ok' field of the $response array
					np1_att = $("#new_pass1").attr("class");
					
					// set the new_pass1 boolean from the 'ok' field of the $response array
					np2_att = $("#new_pass2").attr("class");
					
					// compare the two $response booleans and validate the form and button...
					if ( op_att == 'valid' &&  np1_att == 'valid' && np2_att == 'valid' ) {
						$("#changepassbutton").removeAttr("disabled");
						$("#changepassbutton").attr('onclick','change_password()');
					} else {	// or invalidate the form and button
						$("#changepassbutton").attr("disabled", true);
						$("#changepassbutton").removeAttr("onclick");
					}
					
					// put the 'msg' field from the $response array from check_pass_match (php code)
					// in to the validation message
					validateMatch.html(msg);
				}, 200);
				
				// copy the latest value to avoid sending requests when we don't need to
				t3.lastValue = t3.value;
			}
		}
		/*************************************************************************************************************/

		//	Field event handeling for old_pass change focus, keyup and mouseup
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('#old_pass').keyup(function () {			// Keyup
			// cache the 'this' instance as we need access to it within a setTimeout, where 'this' is set to 'window'
			var t = this;
			verify_oldpass(t);
		});
		/*************************************************************************************************************/

		//	Field event handeling for new_pass1 change focus, keyup and mouseup
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('#new_pass1').keyup(function () {				// Keyup
			// cache the 'this' instance as we need access to it within a setTimeout, where 'this' is set to 'window'
			var t2 = this; 
			validate_newpass(t2);
		});
		/*************************************************************************************************************/
		
		//	Field event handeling for new_pass1 change focus, keyup and mouseup
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$('#new_pass2').keyup(function () {				// Keyup
			// cache the 'this' instance as we need access to it within a setTimeout, where 'this' is set to 'window'
			var t3 = this; 
			check_newpass(t3);
		});
		/*************************************************************************************************************/
		
	});
	
	
//	END EMAIL VERIFICATION FUNCTION
-->