<?php
//	Model functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	

	function build_toolbox_links( $uid ) {
		$sfdb = sfdb();
		$users_pages = $sfdb->get_users_pages( $uid );
		if ( $user_pages ) {
			$page_list = "<li class='page-list-li'>Use ".SITENAME." as:</li>";
			foreach ( $users_pages as $page ) {
				$page_avatar = "<img class='tiny-group-avatar icon-16x16' src='$group->avatar_url'>";
				$page_list .= "<li class='page-list-li'>$page_avatar $page->name</li>\n";
			}
			return $page_list;
		}
	}

	function build_user_toolbox() {
		$user_toolbox_template = file_get_contents( CURRENT_THEME_VIEWS_PATH.'user-toolbox.html' );
		if ( !$user_toolbox_template ) {
			exit;
		} else {
			$SETTINGS_URL = $loggedin_user->domain."/settings";
			$user_toolbox = str_replace( 'SETTINGS_URL', $SETTINGS_URL, $user_toolbox_template );
			
			$ACCOUNT_SETTINGS_ICON_URL = BASE_URL."assets/icons/png/white-settings-256.png";
			$user_toolbox = str_replace( 'ACCOUNT_SETTINGS_ICON_URL', $ACCOUNT_SETTINGS_ICON_URL, $user_toolbox );
			
			
			$EDIT_PROFILE_URL = $loggedin_user->domain."/edit-profile";
			$user_toolbox = str_replace( 'EDIT_PROFILE_URL', $EDIT_PROFILE_URL, $user_toolbox );

			$EDIT_PROFILE_ICON_URL = BASE_URL."assets/icons/png/white-pen-256.png";
			$user_toolbox = str_replace( 'EDIT_PROFILE_ICON_URL', $EDIT_PROFILE_ICON_URL, $user_toolbox );


			$WHITEBOOK_URL = $loggedin_user->domain."/whitebook";
			$user_toolbox = str_replace( 'WHITEBOOK_URL', $WHITEBOOK_URL, $user_toolbox );

			$WHITEBOOK_ICON_URL = BASE_URL."assets/icons/png/whitebook-256.png";
			$user_toolbox = str_replace( 'WHITEBOOK_ICON_URL', $WHITEBOOK_ICON_URL, $user_toolbox );
			
			
			$FRIEND_REQUESTS_URL = $loggedin_user->domain."/friend-requests";
			$user_toolbox = str_replace( 'FRIEND_REQUESTS_URL', $FRIEND_REQUESTS_URL, $user_toolbox );
			
			$REQUESTS_ICON_URL = BASE_URL."assets/icons/png/white-introduction-256.png";
			$user_toolbox = str_replace( 'REQUESTS_ICON_URL', $REQUESTS_ICON_URL, $user_toolbox );
			
			
			$INBOX_URL = $loggedin_user->domain."/messages";
			$user_toolbox = str_replace( 'INBOX_URL', $INBOX_URL, $user_toolbox );
			
			$INBOX_ICON_URL = BASE_URL."assets/icons/png/white-inbox-256.png";
			$user_toolbox = str_replace( 'INBOX_ICON_URL', $INBOX_ICON_URL, $user_toolbox );
			
			
			$NOTIFICATIONS_URL = $loggedin_user->domain."/notifications";
			$user_toolbox = str_replace( 'NOTIFICATIONS_URL', $NOTIFICATIONS_URL, $user_toolbox );
			
			$NOTIFICATIONS_ICON_URL = BASE_URL."assets/icons/png/white-notifications-256.png";
			$user_toolbox = str_replace( 'NOTIFICATIONS_ICON_URL', $NOTIFICATIONS_ICON_URL, $user_toolbox );
			
			
			$EXPANDER_ICON_URL = BASE_URL."assets/icons/png/white-expand-right-256.png";
			$user_toolbox = str_replace( 'EXPANDER_ICON_URL', $EXPANDER_ICON_URL, $user_toolbox );
			
			
			$TOOLBOX_LINKS = build_toolbox_links( $_SESSION['uid'] );
			$user_toolbox = str_replace( 'TOOLBOX_LINKS', $TOOLBOX_LINKS, $user_toolbox );
		}
	}


	function build_profile_page( $user ) {
		$profile_page_template = file_get_contents( CURRENT_THEME_VIEWS_PATH.'profile.html' );
		if ( !$profile_page_template ) {
			exit;
		} else {
			$cover_photo_url = $user->cover_photo;
			$profile_page = str_replace( 'COVER_PHOTO_URL', $cover_photo_url, $profile_page_template );

			$timeline_link = BASE_URL.$user->username."/timeline";
			$profile_page = str_replace( 'TIMELINE_LINK', $timeline_link, $profile_page );

			$about_link = BASE_URL.$user->username."/about";
			$profile_page = str_replace( 'ABOUT_LINK', $about_link, $profile_page );

			$photos_link = BASE_URL.$user->username."/photos";
			$profile_page = str_replace( 'PHOTOS_LINK', $photos_link, $profile_page );

			$videos_link = BASE_URL.$user->username."/videos";
			$profile_page = str_replace( 'VIDEOS_LINK', $videos_link, $profile_page );

			$friends_link = BASE_URL.$user->username."/friends";
			$profile_page = str_replace( 'FRIENDS_LINK', $friends_link, $profile_page );
			
			$profile_avatar = "<img class='profile-avatar-160-160' src='".$user->avatar_url."'>";
			$profile_page = str_replace( 'PROFILE_AVATAR', $profile_avatar, $profile_page );
			
			$names_link = BASE_URL.$user->username;
			$profile_page = str_replace( 'NAMES_LINK', $names_link, $profile_page );
			
			$display_name = $user->display_name;
			$profile_page = str_replace( 'DISPLAYNAME', $display_name, $profile_page );
			
			$alt_display_name = $user->alt_display_name;
			$profile_page = str_replace( 'ALT_DISPLAY_NAME', $alt_display_name, $profile_page );
			
			$profile_actions = "<button>Profile Actions</button>";
			$profile_page = str_replace( 'PROFILE_ACTIONS', $profile_actions, $profile_page );
			
			//$left_boxes = do_left_boxes( $user->uid );
			//$profile_page = str_replace( 'LEFT_BOXES', $left_boxes, $profile_page );
			
			//$timeline_column = do_user_timeline( $user->uid );
			//$profile_page = str_replace( 'TIMELINE_COLUMN', $timeline_column, $profile_page );
		}
		return $profile_page;
	}





	function back_end_header() {
		require_once( CURRENT_THEME_PATH.'back-end-header.model' );
	}
	
	function front_page_header() {
		require_once( CURRENT_THEME_PATH.'front-page-header.model' );
	}





	function get_the_header() {
		require_once( CURRENT_THEME_PATH.'header.php' );
	}
	
	function get_the_content() {
		require_once( CURRENT_THEME_PATH.'content.php' );
	}
	
	function get_the_footer() {
		require_once( CURRENT_THEME_PATH.'footer.php' );
	}





	function do_front_page() {
		require_once( CURRENT_THEME_PATH.'front-page.php' );
	}
	
	function do_front_page_login() {
		require_once( CURRENT_THEME_PATH.'front-page-login.php' );
	}
	
	function do_front_page_footer() {
		require_once( CURRENT_THEME_PATH.'front-page-footer.php' );
	}





	function do_back_end() {
		require_once( CURRENT_THEME_PATH.'back-end.php' );
	}
	
	function do_back_end_footer() {
		require_once( CURRENT_THEME_PATH.'back-end-footer.php' );
	}





	function do_post_box() {
		require_once( CURRENT_THEME_PATH.'postbox.php' );
	}
	
	function do_post_form() {
		require_once( CURRENT_THEME_PATH.'post-form.php' );
	}
	
	function do_sidebar() {
		require_once( CURRENT_THEME_PATH.'sidebar.php' );
	}





	function do_skinny_sidebar() {
		require_once( CURRENT_THEME_PATH.'skinny-sidebar.php' );
	}
	
	function do_phat_sidebar() {
		require_once( CURRENT_THEME_PATH.'phat-sidebar.php' );
	}
	
	function do_live_sidebar() {
		require_once( CURRENT_THEME_PATH.'live-sidebar.php' );
	}
		function get_activity_ticker() {
			require_once( CURRENT_THEME_PATH.'activity-ticker.php' );
		}





	function do_searchbar() {
		require_once( CURRENT_THEME_PATH.'searchbar.php' );
	}
	
	function do_searchbox() {
		require_once( CURRENT_THEME_PATH.'searchbox.php' );
	}
?>