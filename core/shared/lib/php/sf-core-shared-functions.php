<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		sf-core-shared-function.php
//	Path:		core/shared/lib/php/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Shared Functions - Functions that are used globaly in the script - Loaded always
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Returns the Base URL
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function base_url(){
		$protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
		return $protocol . "://" . $_SERVER['HTTP_HOST'];
	}
/***************************************************************************************************************************/
//	Generates a random alpha-numeric string of specified $length ($length defaults to 17)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function gen_rand_str( $length = 17 ) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ( $i = 0; $i < $length; $i++ ) {
			$randomString .= $characters[ rand( 0, strlen( $characters ) - 1 ) ];
		}
		return $randomString;
	}
/***************************************************************************************************************************/
//	File Functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function create_folder( $path, $chmod = 755, $R = false ) {
		if ( !is_dir($path) ) {
			mkdir( $path, $chmod, $R );
		}
	}

	function write_file( $path, $filename, $mode = "r+", $write_data ) {
		$handle = fopen( $path.$filename, $mode );
		fwrite( $handle, $write_data );
		fclose( $handle );
	}
/***************************************************************************************************************************/
//	Checks if the user is logged in - Returns true or false
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_is_user_logged_in() {
		if ( $_SESSION['logged_in'] && isset( $_SESSION['uid'] ) ) {
			return true;
		} else {
			return false;
		}
	}
/***************************************************************************************************************************/
//	Checks if the user is logged in - Returns true or false
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_is_user_admin() {
		if ( $_SESSION['admin'] && isset( $_SESSION['uid'] ) ) {
			return true;
		} else {
			return false;
		}
	}
/***************************************************************************************************************************/
//	Functions for handeling error and event logging
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Function for handeling error logging
	function raise_error( $message ) {
		$serror=
			"Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
			"Timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
			"Script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
			"Error:     " . $message ."\r\n\r\n";
		$date = date('Ymd');
		// Open a log file and write the error
		$fhandle = fopen( ABSPATH.'logs/errors-'.$date.'.log', 'a' );
		if($fhandle){
			$fwr = fwrite( $fhandle, $serror );
			fclose(( $fhandle ));
		}
		// E-mail error to system operator
		mail( SITE_ADMIN_EMAIL, "An Error Occurred! on ".$_SERVER['SERVER_NAME'], $serror, 'From: server@'.NAKED_SITE_URL );
	}

	// Function for handeling event logging
	function log_event($message) {
		// global $system_operator_mail, $system_from_mail;
		// date_default_timezone_set($timezone);											// Set the user's timezone
		$sevent=
		"Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
		"Timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
		"Script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
		"Event:     " . $message ."\r\n\r\n";
		$date = date('Ymd');
		// Open a log file and write the error
		$fhandle = fopen( ABSPATH.'logs/events-'.$date.'.log', 'a' );
		if($fhandle){
			$fwr = fwrite( $fhandle, $sevent );
			fclose(( $fhandle ));
		}
		//if (error == $uid ) { $response = array( 'ok' => false, 'msg' => 'NOT fhandle!' ); return $response; }
		// E-mail error to system operator
		// if(!$b_debugmode)
		//   mail($system_operator_mail, 'error: '.$message, $serror, 'From: ' . $system_from_mail );
	}
/***************************************************************************************************************************/
//	Page part display functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function echo_site_title() {
		echo SITENAME;
	}
	
	function echo_page_title( $seperator ) {
		global $current_page_title;
		if ( $current_page_title != '' ) {
			echo " $seperator ".$current_page_title;
		}
	}
/***************************************************************************************************************************/

	function sf_get_view_header( $view ) {
		switch( $view ) {
			case 'admin' : $view_header = '';
			case 'back-end' : $view_header = '';
			case 'front-end' : $view_header = '';
		}
		return $view_header;
	}
	
	function sf_get_page_title() {
		return SITENAME . '';
	}





	function build_avatar( $l, $w, $uid ) {
		if ( $uid != $_SESSION['uid'] ) {
			$this_user = $sfdb->get_user_by_id( $uid );
			$avatar = '<img class="$lx$w avatar" src="'.$this_user->avatar_url.'">';
		}
		if ( $uid == $_SESSION['uid'] ) {
			$avatar = '<img class="$lx$w avatar" src="'.$loggedin_user->avatar_url.'">';
		}
		return $avatar;
	}




//	Blank
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/***************************************************************************************************************************/

//	FOR DEBUGGING ONLY!
//	Comment out this entire section before uploading to production server.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
/***************************************************************************************************************************/

?>