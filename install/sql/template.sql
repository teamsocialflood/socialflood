--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET search_path = public, pg_catalog;

--
-- Name: update_avatar_time_stamp(); Type: FUNCTION; Schema: public; Owner: socialflood_8s2q
--

CREATE FUNCTION update_avatar_time_stamp() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
    NEW.last_updated := date_trunc('second', now() );
    RETURN NEW;
END;$$;


ALTER FUNCTION public.update_avatar_time_stamp() OWNER TO socialflood_8s2q;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: s0cf_comments; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_comments (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    content_type smallint NOT NULL,
    parent_id bigint NOT NULL,
    comment_content character varying(255) NOT NULL,
    direct_link character varying(255) NOT NULL,
    time_stamp timestamp without time zone DEFAULT now() NOT NULL,
    ip_address character varying(18) NOT NULL,
    privacy smallint DEFAULT 0 NOT NULL,
    restrict smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.s0cf_comments OWNER TO postgres;

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_id_seq OWNER TO postgres;

--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE comments_id_seq OWNED BY s0cf_comments.id;


--
-- Name: s0cf_friends; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_friends (
    id integer NOT NULL,
    initiator_id bigint NOT NULL,
    friend_id bigint NOT NULL,
    confirmed smallint DEFAULT 0 NOT NULL,
    is_limited smallint DEFAULT 1 NOT NULL,
    date_created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.s0cf_friends OWNER TO postgres;

--
-- Name: friends_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE friends_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.friends_id_seq OWNER TO postgres;

--
-- Name: friends_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE friends_id_seq OWNED BY s0cf_friends.id;


--
-- Name: s0cf_group_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_group_stats (
    group_id integer NOT NULL,
    member_count bigint DEFAULT 0 NOT NULL,
    post_count bigint DEFAULT 0 NOT NULL,
    comment_count bigint DEFAULT 0 NOT NULL,
    reply_count bigint DEFAULT 0 NOT NULL,
    age bigint NOT NULL
);


ALTER TABLE public.s0cf_group_stats OWNER TO postgres;

--
-- Name: group_stats_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE group_stats_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_stats_group_id_seq OWNER TO postgres;

--
-- Name: group_stats_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE group_stats_group_id_seq OWNED BY s0cf_group_stats.group_id;


--
-- Name: s0cf_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_groups (
    id integer NOT NULL,
    creator_id bigint NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    privacy smallint NOT NULL
);


ALTER TABLE public.s0cf_groups OWNER TO postgres;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO postgres;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE groups_id_seq OWNED BY s0cf_groups.id;


--
-- Name: s0cf_likes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_likes (
    id integer NOT NULL,
    liked_by_id bigint NOT NULL,
    content_type smallint NOT NULL,
    liked_item_id bigint NOT NULL,
    date_liked timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.s0cf_likes OWNER TO postgres;

--
-- Name: likes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE likes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.likes_id_seq OWNER TO postgres;

--
-- Name: likes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE likes_id_seq OWNED BY s0cf_likes.id;


--
-- Name: s0cf_user_meta_fields; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_user_meta_fields (
    id integer NOT NULL,
    group_id bigint NOT NULL,
    parent_id bigint NOT NULL,
    type smallint NOT NULL,
    name character varying(32) NOT NULL,
    description character varying(255),
    is_required smallint NOT NULL,
    is_default_option smallint NOT NULL,
    field_order smallint NOT NULL,
    option_order smallint NOT NULL,
    order_by smallint NOT NULL,
    can_delete smallint NOT NULL
);


ALTER TABLE public.s0cf_user_meta_fields OWNER TO postgres;

--
-- Name: meta_fields_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE meta_fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meta_fields_id_seq OWNER TO postgres;

--
-- Name: meta_fields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE meta_fields_id_seq OWNED BY s0cf_user_meta_fields.id;


--
-- Name: s0cf_page_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_page_stats (
    page_id integer NOT NULL,
    member_count bigint NOT NULL,
    post_count bigint NOT NULL,
    comment_count bigint NOT NULL,
    reply_count bigint NOT NULL,
    age bigint NOT NULL
);


ALTER TABLE public.s0cf_page_stats OWNER TO postgres;

--
-- Name: page_stats_page_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE page_stats_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_stats_page_id_seq OWNER TO postgres;

--
-- Name: page_stats_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE page_stats_page_id_seq OWNED BY s0cf_page_stats.page_id;


--
-- Name: s0cf_pages; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_pages (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    privacy smallint DEFAULT 0 NOT NULL,
    avatar_url character varying(255) DEFAULT 'none'::character varying NOT NULL
);


ALTER TABLE public.s0cf_pages OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_id_seq OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pages_id_seq OWNED BY s0cf_pages.id;


--
-- Name: s0cf_post_meta; Type: TABLE; Schema: public; Owner: socialflood_8s2q; Tablespace: 
--

CREATE TABLE s0cf_post_meta (
    id integer NOT NULL,
    meta_key character varying(32) NOT NULL,
    meta_value character varying(255) NOT NULL,
    post_id bigint DEFAULT 1 NOT NULL
);


ALTER TABLE public.s0cf_post_meta OWNER TO socialflood_8s2q;

--
-- Name: post_meta_id_seq; Type: SEQUENCE; Schema: public; Owner: socialflood_8s2q
--

CREATE SEQUENCE post_meta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_meta_id_seq OWNER TO socialflood_8s2q;

--
-- Name: post_meta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: socialflood_8s2q
--

ALTER SEQUENCE post_meta_id_seq OWNED BY s0cf_post_meta.id;


--
-- Name: s0cf_posts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_posts (
    id integer NOT NULL,
    owner_id bigint NOT NULL,
    post_content text NOT NULL,
    time_stamp timestamp without time zone DEFAULT now() NOT NULL,
    ip_address character varying(18) NOT NULL,
    privacy smallint DEFAULT 0 NOT NULL,
    restrict smallint DEFAULT 0 NOT NULL,
    post_type character varying(64) DEFAULT 'activity'::character varying NOT NULL,
    owner_display_name character varying(64) NOT NULL
);


ALTER TABLE public.s0cf_posts OWNER TO postgres;

--
-- Name: COLUMN s0cf_posts.post_type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN s0cf_posts.post_type IS 'activity, page, group';


--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE posts_id_seq OWNED BY s0cf_posts.id;


--
-- Name: s0cf_replies; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_replies (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    content_type smallint NOT NULL,
    parent_id bigint NOT NULL,
    reply_content character varying(255) NOT NULL,
    direct_link character varying(255) NOT NULL,
    time_stamp timestamp without time zone DEFAULT now() NOT NULL,
    ip_address character varying(18) NOT NULL,
    privacy smallint DEFAULT 0 NOT NULL,
    restrict smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.s0cf_replies OWNER TO postgres;

--
-- Name: replies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE replies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.replies_id_seq OWNER TO postgres;

--
-- Name: replies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE replies_id_seq OWNED BY s0cf_replies.id;


--
-- Name: s0cf_local_api_tokens; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_local_api_tokens (
    token character varying(156) NOT NULL,
    api_hits smallint DEFAULT 0 NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.s0cf_local_api_tokens OWNER TO postgres;

--
-- Name: s0cf_privacy_codes; Type: TABLE; Schema: public; Owner: socialflood_8s2q; Tablespace: 
--

CREATE TABLE s0cf_privacy_codes (
    id smallint NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE public.s0cf_privacy_codes OWNER TO socialflood_8s2q;

--
-- Name: s0cf_settings; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_settings (
    type character varying(32) DEFAULT 'general'::character varying NOT NULL,
    name character varying(32) NOT NULL,
    description character varying(255),
    value character varying(255) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.s0cf_settings OWNER TO postgres;

--
-- Name: s0cf_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE s0cf_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s0cf_settings_id_seq OWNER TO postgres;

--
-- Name: s0cf_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE s0cf_settings_id_seq OWNED BY s0cf_settings.id;


--
-- Name: s0cf_user_avatars; Type: TABLE; Schema: public; Owner: socialflood_8s2q; Tablespace: 
--

CREATE TABLE s0cf_user_avatars (
    user_id bigint NOT NULL,
    url character varying(255) NOT NULL,
    last_updated timestamp without time zone,
    privacy smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.s0cf_user_avatars OWNER TO socialflood_8s2q;

--
-- Name: s0cf_user_media; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_user_media (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    media_type character varying(32) NOT NULL,
    filename character varying(32) NOT NULL,
    path character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    privacy smallint DEFAULT 0 NOT NULL,
    uploaded_time_stamp timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.s0cf_user_media OWNER TO postgres;

--
-- Name: s0cf_user_meta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_user_meta (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    group_id bigint NOT NULL,
    meta_key character varying(32) NOT NULL,
    meta_content character varying(255) NOT NULL,
    privacy smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.s0cf_user_meta OWNER TO postgres;

--
-- Name: s0cf_user_meta_field_types; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_user_meta_field_types (
    id integer NOT NULL,
    field_type_name character varying NOT NULL
);


ALTER TABLE public.s0cf_user_meta_field_types OWNER TO postgres;

--
-- Name: s0cf_user_meta_groups; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_user_meta_groups (
    id integer NOT NULL,
    group_name character varying(64) NOT NULL,
    description character varying(255),
    group_order smallint NOT NULL,
    can_delete smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.s0cf_user_meta_groups OWNER TO postgres;

--
-- Name: s0cf_user_stats; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_user_stats (
    user_id integer NOT NULL,
    post_count bigint DEFAULT 0 NOT NULL,
    comment_count bigint DEFAULT 0 NOT NULL,
    reply_count bigint DEFAULT 0 NOT NULL,
    age_range character varying(8) DEFAULT (0 - 100) NOT NULL,
    locked smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.s0cf_user_stats OWNER TO postgres;

--
-- Name: s0cf_user_subscriptions; Type: TABLE; Schema: public; Owner: socialflood_8s2q; Tablespace: 
--

CREATE TABLE s0cf_user_subscriptions (
    id integer NOT NULL,
    user_id bigint NOT NULL,
    subscription_type character varying(64) DEFAULT 'activity'::character varying NOT NULL,
    resource_id bigint NOT NULL,
    resource_name character varying(64) NOT NULL,
    subscribed_date timestamp without time zone DEFAULT now() NOT NULL,
    confirmed smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.s0cf_user_subscriptions OWNER TO socialflood_8s2q;

--
-- Name: COLUMN s0cf_user_subscriptions.subscription_type; Type: COMMENT; Schema: public; Owner: socialflood_8s2q
--

COMMENT ON COLUMN s0cf_user_subscriptions.subscription_type IS 'group, page, activity';


--
-- Name: s0cf_user_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: socialflood_8s2q
--

CREATE SEQUENCE s0cf_user_subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s0cf_user_subscriptions_id_seq OWNER TO socialflood_8s2q;

--
-- Name: s0cf_user_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: socialflood_8s2q
--

ALTER SEQUENCE s0cf_user_subscriptions_id_seq OWNED BY s0cf_user_subscriptions.id;


--
-- Name: s0cf_user_subscriptions_resource_id_seq; Type: SEQUENCE; Schema: public; Owner: socialflood_8s2q
--

CREATE SEQUENCE s0cf_user_subscriptions_resource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s0cf_user_subscriptions_resource_id_seq OWNER TO socialflood_8s2q;

--
-- Name: s0cf_user_subscriptions_resource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: socialflood_8s2q
--

ALTER SEQUENCE s0cf_user_subscriptions_resource_id_seq OWNED BY s0cf_user_subscriptions.resource_id;


--
-- Name: s0cf_user_subscriptions_resource_name_seq; Type: SEQUENCE; Schema: public; Owner: socialflood_8s2q
--

CREATE SEQUENCE s0cf_user_subscriptions_resource_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s0cf_user_subscriptions_resource_name_seq OWNER TO socialflood_8s2q;

--
-- Name: s0cf_user_subscriptions_resource_name_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: socialflood_8s2q
--

ALTER SEQUENCE s0cf_user_subscriptions_resource_name_seq OWNED BY s0cf_user_subscriptions.resource_name;


--
-- Name: s0cf_user_subscriptions_subscription_type_seq; Type: SEQUENCE; Schema: public; Owner: socialflood_8s2q
--

CREATE SEQUENCE s0cf_user_subscriptions_subscription_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s0cf_user_subscriptions_subscription_type_seq OWNER TO socialflood_8s2q;

--
-- Name: s0cf_user_subscriptions_subscription_type_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: socialflood_8s2q
--

ALTER SEQUENCE s0cf_user_subscriptions_subscription_type_seq OWNED BY s0cf_user_subscriptions.subscription_type;


--
-- Name: s0cf_user_subscriptions_user_id_seq; Type: SEQUENCE; Schema: public; Owner: socialflood_8s2q
--

CREATE SEQUENCE s0cf_user_subscriptions_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s0cf_user_subscriptions_user_id_seq OWNER TO socialflood_8s2q;

--
-- Name: s0cf_user_subscriptions_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: socialflood_8s2q
--

ALTER SEQUENCE s0cf_user_subscriptions_user_id_seq OWNED BY s0cf_user_subscriptions.user_id;


--
-- Name: s0cf_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE s0cf_users (
    id integer NOT NULL,
    email character varying(64) NOT NULL,
    password character varying(255) NOT NULL,
    login_token character varying DEFAULT 0 NOT NULL,
    joined_time_stamp timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.s0cf_users OWNER TO postgres;

--
-- Name: user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_groups_id_seq OWNER TO postgres;

--
-- Name: user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_groups_id_seq OWNED BY s0cf_user_meta_groups.id;


--
-- Name: user_media_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_media_id_seq OWNER TO postgres;

--
-- Name: user_media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_media_id_seq OWNED BY s0cf_user_media.id;


--
-- Name: user_meta_field_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_meta_field_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_meta_field_types_id_seq OWNER TO postgres;

--
-- Name: user_meta_field_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_meta_field_types_id_seq OWNED BY s0cf_user_meta_field_types.id;


--
-- Name: user_meta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_meta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_meta_id_seq OWNER TO postgres;

--
-- Name: user_meta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_meta_id_seq OWNED BY s0cf_user_meta.id;


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY s0cf_users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_friends ALTER COLUMN id SET DEFAULT nextval('friends_id_seq'::regclass);


--
-- Name: group_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_group_stats ALTER COLUMN group_id SET DEFAULT nextval('group_stats_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_likes ALTER COLUMN id SET DEFAULT nextval('likes_id_seq'::regclass);


--
-- Name: page_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_page_stats ALTER COLUMN page_id SET DEFAULT nextval('page_stats_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_pages ALTER COLUMN id SET DEFAULT nextval('pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: socialflood_8s2q
--

ALTER TABLE ONLY s0cf_post_meta ALTER COLUMN id SET DEFAULT nextval('post_meta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_replies ALTER COLUMN id SET DEFAULT nextval('replies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_settings ALTER COLUMN id SET DEFAULT nextval('s0cf_settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_user_media ALTER COLUMN id SET DEFAULT nextval('user_media_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_user_meta ALTER COLUMN id SET DEFAULT nextval('user_meta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_user_meta_field_types ALTER COLUMN id SET DEFAULT nextval('user_meta_field_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_user_meta_fields ALTER COLUMN id SET DEFAULT nextval('meta_fields_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_user_meta_groups ALTER COLUMN id SET DEFAULT nextval('user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: socialflood_8s2q
--

ALTER TABLE ONLY s0cf_user_subscriptions ALTER COLUMN id SET DEFAULT nextval('s0cf_user_subscriptions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY s0cf_users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: friends_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_friends
    ADD CONSTRAINT friends_pkey PRIMARY KEY (id);


--
-- Name: group_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_group_stats
    ADD CONSTRAINT group_stats_pkey PRIMARY KEY (group_id);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: likes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_likes
    ADD CONSTRAINT likes_pkey PRIMARY KEY (id);


--
-- Name: page_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_page_stats
    ADD CONSTRAINT page_stats_pkey PRIMARY KEY (page_id);


--
-- Name: pages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: posta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_posts
    ADD CONSTRAINT posta_pkey PRIMARY KEY (id);


--
-- Name: profile_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_user_meta_fields
    ADD CONSTRAINT profile_fields_pkey PRIMARY KEY (id);


--
-- Name: profile_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_user_meta_groups
    ADD CONSTRAINT profile_groups_pkey PRIMARY KEY (id);


--
-- Name: replies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_replies
    ADD CONSTRAINT replies_pkey PRIMARY KEY (id);


--
-- Name: s0cf_local_api_auth_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_local_api_tokens
    ADD CONSTRAINT s0cf_local_api_auth_pkey PRIMARY KEY (token);


--
-- Name: s0cf_post_meta_pkey; Type: CONSTRAINT; Schema: public; Owner: socialflood_8s2q; Tablespace: 
--

ALTER TABLE ONLY s0cf_post_meta
    ADD CONSTRAINT s0cf_post_meta_pkey PRIMARY KEY (id);


--
-- Name: s0cf_privacy_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: socialflood_8s2q; Tablespace: 
--

ALTER TABLE ONLY s0cf_privacy_codes
    ADD CONSTRAINT s0cf_privacy_codes_pkey PRIMARY KEY (id);


--
-- Name: s0cf_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_settings
    ADD CONSTRAINT s0cf_settings_pkey PRIMARY KEY (id);


--
-- Name: s0cf_user_avatars_pkey; Type: CONSTRAINT; Schema: public; Owner: socialflood_8s2q; Tablespace: 
--

ALTER TABLE ONLY s0cf_user_avatars
    ADD CONSTRAINT s0cf_user_avatars_pkey PRIMARY KEY (user_id);


--
-- Name: s0cf_user_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: socialflood_8s2q; Tablespace: 
--

ALTER TABLE ONLY s0cf_user_subscriptions
    ADD CONSTRAINT s0cf_user_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: user_media_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_user_media
    ADD CONSTRAINT user_media_pkey PRIMARY KEY (id);


--
-- Name: user_meta_field_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_user_meta_field_types
    ADD CONSTRAINT user_meta_field_types_pkey PRIMARY KEY (id);


--
-- Name: user_meta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_user_meta
    ADD CONSTRAINT user_meta_pkey PRIMARY KEY (id);


--
-- Name: user_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_user_stats
    ADD CONSTRAINT user_stats_pkey PRIMARY KEY (user_id);


--
-- Name: users_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY s0cf_users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: avatar_last_update_trigger; Type: TRIGGER; Schema: public; Owner: socialflood_8s2q
--

CREATE TRIGGER avatar_last_update_trigger BEFORE INSERT OR UPDATE ON s0cf_user_avatars FOR EACH ROW EXECUTE PROCEDURE update_avatar_time_stamp();


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--