<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		installer-functions.php
//	Path:		/install/includes/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Create a new PDO database object
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$install_database = file_get_contents( $_SERVER['DOCUMENT_ROOT'].'/install/sql/template.sql' );
	
	define( 'INSTALL_QUERY', str_replace('sf_', TP.'_', $install_database ) );

	function write_config_file( $post ) {
		$config_file =
		"<?php\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"//\tProject:\tSocialFlood Social Networking Engine"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"//\tFile:\t\tconfig.php"."\n".
		"//\tPath:\t\t/"."\n".
		"//\tVersion:\t0.0.1"."\n".
		"//\tUpdated:\t3/28/2013"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		""."\n".
		"//\tConfiguration Settings"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"\t// The type of database you are using. (mysql or pgsql)"."\n".
		"\t".'$db_type'."\t\t= 'pgsql';"."\n".
		"\t// The database user you created."."\n".
		"\t".'$db_user'."\t\t= '".$post['db_user']."';\n".
		"\t// The name of your database."."\n".
		"\t".'$db_name'."\t\t= '".$post['db_name']."';\n".
		"\t// The password to your database."."\n".
		"\t".'$db_pass'."\t\t= '".$post['db_password']."';\n".
		"\t// The databade host address. (usually localhost)"."\n".
		"\t".'$db_host'."\t\t= '".$post['db_host']."';\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		""."\n".
		"//\tIf you would like to change the default database table prefix for obscuration then do so here."."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"\t".'$table_prefix = "'.$post['table_prefix'].'"; // Set your desired table prefix here.'."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		""."\n".
		"//\tError Reporting"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"\t// Setting true enables error control messages to be seen"."\n".
		"\t// Setting false hides error control messages"."\n".
		"\t".'$error_control = true;'."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		""."\n".
		""."\n".
		""."\n".
		""."\n".
		""."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"//\tDO NOT EDIT BELOW THIS LINE!"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		""."\n".
		"//\tDefine The Database Constants"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"\t".'define( "DB_TYPE", $db_type );'."\n".
		"\t".'define( "DB_USER", $db_user );'."\n".
		"\t".'define( "DB_NAME", $db_name );'."\n".
		"\t".'define( "DB_PASS", $db_pass );'."\n".
		"\t".'define( "DB_HOST", $db_host );'."\n".
		"\tdefine( 'DB_PORT', '5432' );"."\n".
		"\tdefine( 'DB_CHARSET', 'utf8' );"."\n".
		"\tdefine( 'DB_COLLATE', 'null' );"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		""."\n".
		"//\tDefine The TABLE_PREFIX"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"\t".'define( "TP", $table_prefix );'."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		""."\n".
		"// Define the Error Control & Reporting (ERR_CTRL) constant"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"\t".'define( "ERR_CTRL", $error_control );'."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		""."\n".
		"//\tError Control"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"\tif ( ERR_CTRL ) {"."\n".
		"\t\t//error_reporting( 'E_ALL' );"."\n".
		"\t\tini_set( 'display_errors', 1 );"."\n".
		"\t\terror_reporting( ~0 ^ E_NOTICE );"."\n".
		"\t} elseif ( !ERR_CTRL ) {"."\n".
		"\t\terror_reporting( 0 );"."\n".
		"\t\t//echo 'Error Reporting is turned off.<br>';"."\n".
		"\t}"."\n".
		"/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"."\n".
		"?>";
		
		if ( $_POST['step'] == '1' ) {
			$file_path_name = $_SERVER['DOCUMENT_ROOT']."/admin/config.php";
			$myFile = $file_path_name;
			$fh = fopen($myFile, 'w') or die("Can't write config file! Check /admin folder permissions.");
			fwrite($fh, $config_file);
			fclose($fh);
			if ( isset( $_SESSION['global_install'] ) ) {
				unset( $_SESSION['global_install'] ); }
			$_SESSION['step'] = '2';
		}
		
		if ( $_POST['step'] == '2' ) {
			$file_path_names[1] = $_SERVER['DOCUMENT_ROOT']."/api/local/config.php";
			$file_path_names[2] = $_SERVER['DOCUMENT_ROOT']."/api/remote/config.php";
			foreach ( $file_path_names as $file_path_name ) {
				$myFile = $file_path_name;
				$fh = fopen($myFile, 'w') or die("Can't write config file! Check /admin folder permissions.");
				fwrite($fh, $config_file);
				fclose($fh);;
			}
			$_SESSION['step'] = '3';
		}
		
		if ( $_POST['step'] == '3' ) {
			if ( $_POST['global'] ) {
				$file_path_names[0] = $_SERVER['DOCUMENT_ROOT']."/config.php";
				$file_path_names[1] = $_SERVER['DOCUMENT_ROOT']."/api/local/config.php";
				$file_path_names[2] = $_SERVER['DOCUMENT_ROOT']."/api/remote/config.php";
				$file_path_names[3] = $_SERVER['DOCUMENT_ROOT']."/admin/config.php";
				foreach ( $file_path_names as $file_path_name ) {
					$myFile = $file_path_name;
					$fh = fopen($myFile, 'w') or die("Can't write config file! Check /admin folder permissions.");
					fwrite($fh, $config_file);
					fclose($fh);
					$_SESSION['global_install'] = true;
				}
				$_SESSION['step'] = '4';
			} else {
				$file_path_name = $_SERVER['DOCUMENT_ROOT']."/config.php";
				$myFile = $file_path_name;
				$fh = fopen($myFile, 'w') or die("Can't write config file! Check /admin folder permissions.");
				fwrite($fh, $config_file);
				fclose($fh);
				$_SESSION['step'] = '4';
			}
		}
	}
		// Trigger the above function.
		if ( $_POST['write_config'] ) {
			write_config_file( $_POST );
		}
	
	
	
	
	
	function delete_config_file( $step ) {
		if ( $step == '2' ) {
			unlink( $_SERVER['DOCUMENT_ROOT']."/admin/config.php" );
			$_SESSION['step'] = '1';
		}
		if ( $step == '3' ) {
			unlink( $_SERVER['DOCUMENT_ROOT']."/api/local/config.php" );
			unlink( $_SERVER['DOCUMENT_ROOT']."/api/remote/config.php" );
			$_SESSION['step'] = '2';
		}
		
		if ( $step == '4' ) {
			if ( isset( $_SESSION['global_install'] ) ) {
				unlink( $_SERVER['DOCUMENT_ROOT']."/config.php" );
				unlink( $_SERVER['DOCUMENT_ROOT']."/api/local/config.php" );
				unlink( $_SERVER['DOCUMENT_ROOT']."/api/remote/config.php" );
				unlink( $_SERVER['DOCUMENT_ROOT']."/admin/config.php" );
				$_SESSION['step'] = '1';
			} else {
				unlink( $_SERVER['DOCUMENT_ROOT']."/config.php" );
				$_SESSION['step'] = '3';
			}
		}
		if ( $step == '5' ) {
			$sfdb = sfdb();
			$response = $sfdb->empty_database();
			if ( $response['ok'] ) {
				$_SESSION['step'] = '4';
			} else {
				$_SESSION['step'] = '5';
				echo "<h1>Someone try to steal teh launch codez!</h1>";
				echo $response['msg'];
			}
		}
	}
		if ( $_POST['go_back'] ) {
			delete_config_file( $_POST['step'] );
		}





	if ( $_POST['save_settings'] ) {
		//echo $_SERVER['HTTP_HOST'];
		//exit;
		$sfdb = sfdb();
		$response = $sfdb->install_database();
		if ( $response['ok'] ) {
			echo $response['msg'];
			$response = $sfdb->add_site_settings( $_POST );
			if ( $response['ok'] ) {
				$_SESSION['step'] = '5';
			}
		} else {
			$_SESSION['step'] = '4';
			echo "<h1>Someone try to steal teh launch codez!</h1>";
			echo $response['msg'];
			
		}
		
	}
	
	if ( $_POST['finish'] ) {
		$sfdb = sfdb();
		$response = $sfdb->add_god_tier( $_POST );
		if ( $response['ok'] ) {
			$_SESSION['step'] = 'finished';
			echo $response['msg']."<br>";
			echo "<h3>Now, delete the install directory or move it somewhere inaccessible by the interwebz!</h3>";
			echo "Next, <a href='http://".$_SERVER['HTTP_HOST']."'>Login to your ".SITENAME." God Tier Account</a>.";
		} else {
			$_SESSION['step'] = '5';
			echo "<h1>Someone try to steal teh launch codez!</h1>";
			echo $response['msg'];
			
		}
	}
		
	if ( $_SESSION['step'] ) {
		require_once($_SERVER['DOCUMENT_ROOT'].'/install/screens/screen-'.$_SESSION['step'].'.html');
	} else {
		require_once($_SERVER['DOCUMENT_ROOT'].'/install/screens/screen-1.html');
	}	
?>