<?php
	ini_set( 'display_errors', 1 );
	error_reporting( ~0 );

	session_start();
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <title>SocialFlood - Setup Wizard</title>
		<script language="javascript" type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<script language="javascript" type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	</head>
	<body>
<?php
	if ( file_exists( $_SERVER['DOCUMENT_ROOT'].'/config.php') ) {
		require_once( $_SERVER['DOCUMENT_ROOT'].'/config.php');
		require_once( $_SERVER['DOCUMENT_ROOT'].'/includes/classes/pdo.clss');
		require_once( $_SERVER['DOCUMENT_ROOT'].'/includes/classes/db-conn-factory.clss');
	}
	//require_once('includes/upload-file.php');
	require_once('includes/installer-functions.php');
?>
    </body>
    </html>
