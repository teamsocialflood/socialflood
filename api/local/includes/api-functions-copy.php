<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		api-functions.php
//	Path:		/api/local/includes/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo "API Function Loaded!";
//	Returns the Base URL
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	function base_url(){
//		$protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
//		return $protocol . "://" . $_SERVER['HTTP_HOST'];
//	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	File Functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function create_folder( $path, $chmod = 755, $R = false ) {
		if ( !is_dir($path) ) {
			mkdir( $path, $chmod, $R );
		}
	}

	function write_file( $path, $filename, $mode = "r+", $write_data ) {
		$handle = fopen( $path.$filename, $mode );
		fwrite( $handle, $write_data );
		fclose( $handle );
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Generates a random alpha-numeric string of specified $length ($length defaults to 17)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function gen_rand_str( $length = 17 ) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Function for handeling error logging
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function raise_error($message) {
		// global $system_operator_mail, $system_from_mail;
		// date_default_timezone_set($timezone);											// Set the user's timezone
		$serror=
		"Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
		"Timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
		"Script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
		"Error:     " . $message ."\r\n\r\n";
		$date = date('Ymd');
		// Open a log file and write the error
		$fhandle = fopen( ABSPATH.'logs/errors-'.$date.'.log', 'a' );
		if($fhandle){
			$fwr = fwrite( $fhandle, $serror );
			fclose(( $fhandle ));
		}
		//if (error == $uid ) { $response = array( 'ok' => false, 'msg' => 'NOT fhandle!' ); return $response; }
		// E-mail error to system operator
		// if(!$b_debugmode)
		//   mail($system_operator_mail, 'error: '.$message, $serror, 'From: ' . $system_from_mail );
	}
/***************************************************************************************************************************/

//	Function for handeling event logging
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_event($message) {
		// global $system_operator_mail, $system_from_mail;
		// date_default_timezone_set($timezone);											// Set the user's timezone
		$sevent=
		"Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
		"Timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
		"Script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
		"Event:     " . $message ."\r\n\r\n";
		$date = date('Ymd');
		// Open a log file and write the error
		$fhandle = fopen( ABSPATH.'logs/events-'.$date.'.log', 'a' );
		if($fhandle){
			$fwr = fwrite( $fhandle, $sevent );
			fclose(( $fhandle ));
		}
		//if (error == $uid ) { $response = array( 'ok' => false, 'msg' => 'NOT fhandle!' ); return $response; }
		// E-mail error to system operator
		// if(!$b_debugmode)
		//   mail($system_operator_mail, 'error: '.$message, $serror, 'From: ' . $system_from_mail );
	}
/***************************************************************************************************************************/


//	Function that checks whether the given email is valid.
//	Since 0.0.1
//	Param string $email Email.
//	Return boolean True on success, and False on failure.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function is_email( $email ) {
		if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
			return true;
		}
		else {
			return false;
		}
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Function that checks whether the given email is being used.
//	Since 0.0.1
//	Param string $username Username.
//	Return boolean True on success, and False on failure.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function email_exists( $email ) {
		$sfdb = sfdb();
		$dbq = $sfdb->prepare( "SELECT * FROM sf_users WHERE email = '".$email."'" );
		$dbq->execute();
		//$dbq->fetch( PDO::FETCH_ASSOC );
		$row_count = $dbq->rowCount();
		if ( $row_count != 0 ) {
			return true;
		} elseif ( $row_count == 0 ) {
			return false;
		}
	}
/***************************************************************************************************************************/




//	Function that checks whether the given username is taken.
//	Since 0.0.1
//	Param string $username Username.
//	Return boolean True on success, and False on failure.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function username_taken( $username ) {
		$sfdb = sfdb();
		$dbq = $sfdb->prepare( "SELECT * FROM sf_user_meta WHERE meta_key='username' AND meta_content = '".$username."'" );
		$dbq->execute();
		$row_count = $dbq->rowCount();
		echo $row_count;
		//echo $row_count;
		if ( $row_count != 0 ) {
			return true;
		} elseif ( $row_count == 0 ) {
			return false;
		}
	}
/***************************************************************************************************************************/

//	Function for checking a username is valid and available
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function lm_check_username( $username = '' ) {
		/*$response = array(
				'ok'	=> true, 
				'msg'	=> "Axax is working.");
		
		return $response;*/
		
		//$username = trim($username); // strip any white space
		$response = array(); // our response
		// if the username is blank
		if ( $username =='' ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Empty");
		// if the username does not match a-z or '.', '-', '_' then it's not valid
		} else if ( !preg_match( '/^[a-z0-9.-_]+$/', $username ) ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Only use a-Z, 0-9, or .-_");
		// this would live in an external library just to check if the username is taken
		} else if ( username_taken( $username ) ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Taken");
		// it's all good
		} else {
			$response = array(
				'ok'	=> true, 
				'msg'	=> "Available");
		}
		return $response;
	}
/***************************************************************************************************************************/

//	Run username validation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ( $_POST['check_username'] && $_POST['ajax'] == 'yes' ) {
		echo "Recieved the input";
		echo json_encode( lm_check_username( $_POST['check_username'] ) );
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/





//	Function for checking an email is valid and unused
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function lm_check_email( $email ) {
	if ( !is_email( $email ) ) {
		$response = array(
			'ok'	=> false, 
			'msg'	=> 'Invalid email address');
	} else if ( is_email( $email ) ) {
		if ( email_exists( $email ) ) {
			$response = array(
				'ok'	=> false,
				'msg'	=> 'Only one account per email is allowed');
		} else if ( !email_exists( $email ) ) {
			$response = array(
				'ok'	=> true,
				'msg'	=> 'Validated: '.$email );
		}
	}
	return $response;
}
/***************************************************************************************************************************/

//	Function that sends an email verification link
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_send_email_verification_link( $email, $link_code ) {
		$verification_link = "<a href='".BASE_URL."verify/?email=".$email."&verification_code=".$link_code."'>Verify Me!</a>";
		// Email the user a verification link
		$headers	=	'From: Lurkable.me Activations <support@lurkable.me>'."\r\n";
		$subject	=	'Your New Lurkable Account - Please verify your email address.';
		$message	=	"<p><a href='".BASE_URL."'><img src='".BASE_URL."assets/logo/png/".SMALL_SITE_LOGO."'></a></p>".
						"<p>Hello,</p>".													// Activation email message
						"<p>To verify your email address please click the link below.</p>".
						"<p>$verification_link</p>".										// Verification link
						"<p>Thank you,<br>".												// Email closing
						'Lurkable Support Team'."</p>";										// Signature
		mail( $email, $subject, $message, $headers );										// Send the verification email
	}
/***************************************************************************************************************************/

//	Helper Function for registering a new user's account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_core_signup_user( $username, $password, $email, array $usermeta ) {
		$password = sha1( md5( $password ) );
		$display_name = $usermeta['fname']." ".$usermeta['lname'];
		$values1 = "'$email','$username','".$usermeta['fname']."','".$usermeta['lname']."','$display_name','$password'";
		$sfdb = sfdb();
		try {  
			$sfdb->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$sfdb->beginTransaction();
			$sfdb->exec( "INSERT INTO sf_users (email, username, fname, lname, display_name, password) VALUES ($values1)" );
			$user_id = $sfdb->lastInsertId();
			$values2 = "$user_id,'bday','".$usermeta['bday']."','public'";
			$values3 = "$user_id,'bmonth','".$usermeta['bmonth']."','public'";
			$values4 = "$user_id,'byear','".$usermeta['byear']."','public'";
			$values5 = "$user_id,'gender','".$usermeta['gender']."','public'";
			$sfdb->exec( "INSERT INTO sf_user_meta (user_id, meta_key, meta_value, privacy) VALUES ($values2)" );
			$sfdb->exec( "INSERT INTO sf_user_meta (user_id, meta_key, meta_value, privacy) VALUES ($values3)" );
			$sfdb->exec( "INSERT INTO sf_user_meta (user_id, meta_key, meta_value, privacy) VALUES ($values4)" );
			$sfdb->exec( "INSERT INTO sf_user_meta (user_id, meta_key, meta_value, privacy) VALUES ($values5)" );
			$sfdb->commit();
		} catch ( Exception $e ) {
			define( 'DB_CONNECTION_ERR', $e->getMessage() );
			$sfdb->rollBack();
			$response = array(
				'ok'	=>	false,
				'msg'	=>	DB_CONNECTION_ERR );
			echo json_encode( $response );
			exit;
		}
		$link_code = md5( sha1( $email.$username.$password ) );
		sf_send_email_verification_link( $email , $link_code );
		return true;
	}
/***************************************************************************************************************************/

//	Helper Function for registering a new user's account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_new_core_signup_user( $username, $password, $email, array $usermeta )
	{
			//get the pdo object in a evil, wicked way
			$sfdb = sfdb();
	 
			//some cultures put the family name first.  Even though this isn't supported yet in external code, I've made it backwards compatible
			if ( !isset( $usermeta['family_first'] ) )
			{
					$usermeta['family_first'] = FALSE;
			}
	 
			try {  
					$sfdb->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );   //this should be set up before you do anything
					$sfdb->beginTransaction();
	 
					//insert the user safely, sanitize all data, and get the id back
					$initial = $sfdb->prepare( "INSERT INTO `sf_users` (`email`, `username`, `fname`, `lname`, `display_name`, `password`)".
						" VALUES (:email, :username, :fname, :lname, :display_name, :password)" );
					$initial->execute(array(
							':email'=>$email,
							':username'=>$username,
							':fname'=>$usermeta['fname'],
							':lname'=>$usermeta['lname'],
							':display_name'=>($usermeta['family_first'])?$usermeta['lname']." ".$usermeta['fname']:$usermeta['fname']." ".$usermeta['lname'],
							':password'=>sha1( md5( $password ) )
					));
					$user_id = $sfdb->lastInsertId();
	 
					//prepare the meta query
					$meta_query = $sfdb->prepare( "INSERT INTO sf_user_meta (user_id, meta_key, meta_value, privacy) VALUES (:user_id, :meta_key, :meta_value, :privacy)" );
	 
					//since this is a key/val table, lets establish an easy "base" array for data
					$base_data = array(
							':user_id'=>$user_id,
							':privacy'=>'public'
					);
	 
					//since the usermeta array is also key/values, we can loop through it and save some time, which is schweet
					foreach($usermeta as $key=>$value)
					{
							$base_data['meta_key'] = $key; 
							$base_data['meta_value'] = $value;
							$meta_query->execute($base_data);
					}
	 
					//commit it all
					$sfdb->commit();
			}
			catch ( Exception $e )
			{
					define( 'DB_CONNECTION_ERR', $e->getMessage() );
					$sfdb->rollBack();
					$response = array(
							'ok'    =>      false,
							'msg'   =>      DB_CONNECTION_ERR
					);
					echo json_encode( $response );
					exit;
			}
	 
			$link_code = md5( sha1( $email.$username.$password ) );
			sf_send_email_verification_link( $email , $link_code );
			return true;
	}
/***************************************************************************************************************************/

//	Function for registering a new user's account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function lm_register_user($post) {
		$post = (object) $post;
		if ($post->username != '' )	{ $un = true; } else { $un = false; }
		if ($post->email != '') { $em = true; } else { $em = false; }
		if ($post->email2 != '') { $em2 = true; } else { $em2 = false; }
		if ($post->email == $post->email2) { $emm = true; } else { $emm = false; }
		if (strlen($post->password) > 7 ) { $pw = true; } else { $pw = false; }
		if ($post->fname != '') { $fn = true; } else { $fn = false; }
		if ($post->lname != '') { $ln = true; } else { $ln = false; }
		if ($post->byear != '') { $by = true; } else { $by = false; }
		if ($post->bmonth != '') { $bm = true; } else { $bm= false; }
		if ($post->bday != '') { $bd = true; } else { $bd = false; }
		if ($post->gender != '') { $gd = true; } else { $gd = false; }
		if ($post->ajax == 'yes') { $aj = true; } else { $aj = false; }
		
		if ($aj) {
			$usermeta = array(	'fname'		=> $post->fname,
								'lname'		=> $post->lname,
								'bday'		=> $post->bday,
								'bmonth'	=> $post->bmonth,
								'byear'		=> $post->byear,
								'gender'	=> $post->gender );			
			$i = sf_core_signup_user( $post->username, $post->password, $post->email, $usermeta );
			if ( $i )	{
				$response = array(
					'ok'		=>	true,
					'msg'		=>	'Thank you for signing up at Lurkable.<br>'.
									'An email verification link has been sent to '.
									$post->email.'.<br>'.'Please check your email to complete the signup.' );
			} elseif ( !$i ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	"An internal error has occured. It's not your fault. The error was reported!<br>" );
			}
		} elseif ( !$aj ) {
			if ($un && $em && $em2 && $emm && $pw && $fn && $ln && $by && $bm && $bd && $gd) {
				$usermeta = array(	'fname'		=> $post->fname,
									'lname'		=> $post->lname,
									'bday'		=> $post->bday,
									'bmonth'	=> $post->bmonth,
									'byear'		=> $post->byear,
									'gender'	=> $post->gender );		
				$i = sf_core_signup_user( $post->username, $post->password, $post->email, $usermeta );
				if ( $i )	{
					$response = array(
						'ok'		=>	true,
						'msg'		=>	'Thank you for signing up at Lurkable.<br>'.
										'An email verification link has been sent to '.
										$post->email.'.<br>'.'Please check your email to complete the signup.' );
				} elseif ( !$i ) {
					$response = array(
						'ok'		=>	false,
						'msg'		=>	"An internal error has occured. It's not your fault. The error was reported!<br>" );
				}
			} elseif ( !$un ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please enter your desired username.<br>' );
					return $response;
			} elseif ( !$em ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your email.<br>' );
					return $response;
			} elseif ( !$em2 ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must re-enter your email.<br>' );
					return $response;
			} elseif ( !$emm ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'The email fields must match exactly.<br>' );
					return $response;
			} elseif ( !$pw ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Password must be 8+ characters using a-Z, 0-9, [.-_].<br>' );
					return $response;
			} elseif ( !$fn ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please provide your real first name.<br>' );
					return $response;
			} elseif ( !$ln ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please provide your real last name.<br>' );
					return $response;
			} elseif ( !$by ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth year.<br>' );
					return $response;
			} elseif ( !$bm ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth month.<br>' );
					return $response;
			} elseif ( !$bd ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth day.<br>' );
					return $response;
			} elseif ( !$gd ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please select your gender.<br>' );
					return $response;
			}
		}
		return $response;	
	}
/***************************************************************************************************************************/

//	Activate a useres login account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function activate_user($code = '', $user = '', $extra = '') {
		$sfdb = sfdb();
		if ($code != '' && $user != '' && $extra != '') {
			$fusername			= $user.'_'.$code;
			$userdata			= get_user_by('login', $fusername);
			$uid				= $userdata->ID; // the id of the user
			$email				= $userdata->user_email;
			$password			= wp_generate_password(16, false, false);
			$login_link			= "<a href='http://lmutual.com/login'>Login Here</a>";
			$change_pass_link	= "<a href='http://lmutual.com/change-password'>Click Here</a>";
			$userdata2 = array(
				'user_login'	=>	$user, );
			$userdata3 = array(
				'ID'			=>	$uid,
				'user_pass'		=>	$password,
				'user_nicename' =>	$user,
				'display_name'	=>	$user );
			$wpdb->update($wpdb->users, $userdata2, array('ID' => $uid));
			$rid = wp_update_user( $userdata3 );
			// Email the user for verification
			$headers	=	'From: Lurkable.me Activations <support@lurkable.me>'."\r\n";
			$subject	=	'Your New Lurkable Account - Your account has been activated.';
			$message	=	"<p>Hello,</p>".													// Activation email message
							"<p>Your email has been verified and your login has been activated.</p>".
							"<p>You can login and setup your account using these credentials.</p>".
							"<p>Username: ".$user."<br>".										// The login username
							'Password: '.$password."</p>".										// The user's password
							"<p>".$login_link."</p>".
							"<p>You can always change your password under account settings.</p>".
							"<p>".$change_pass_link."</p>".
							"<p>Happy lurking,<br>".											// Email closing
							'Lurkable Support Team'."</p>";										// Signature
			mail( $email, $subject, $message, $headers );										// Send the verification email
			$creds = array(
				'user_login'	=> $user,
				'user_password'	=> $password,
				'remember'		=> true );
			if ( $rid == $uid) { $user_logged = wp_signon( $creds, true ); }
			if (is_wp_error($user_logged)) { $login_error = $user_logged->get_error_message();
				$errorM	=	'There was an error automatically logging in the user, '.
							$user.', while completing the email verification and user activation process. - Error: '.
							$login_error;
				raise_error ($errorM);
				$response	= array (
					'ok'	=> false,
					'msg'	=> 'There was an error automatically logging you in.' );
			} else {
				$response	=	array (
					'ok'	=>	true,
					'msg'	=>	'Your email has been verified.'."<br>".
								'You have been automatically logged in.'."<br>".
								'A temporary password has been emailed to: '.$userdata->user_email."<br>".
								'However, you should change your password to something memorable.'."<br>".
								'If you are not automatically redirected in a few seconds '.$change_pass_link.'.' );
			}
		} else {
			$response	= array (
				'ok'	=> false,
				'msg'	=> 'Invalid request.' );
		}
		return $response;
	}
/***************************************************************************************************************************/

//	Function to validate new password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function validate_new_pass ($new_pass) {
		if ( preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[0-9A-Za-z!@#$%^*]{12,64}$/', $new_pass) ) {
			$response = array(
				'ok'	=> true,
				'msg'	=> '<p>Congratulations your password is strong!</p>');
			return $response;
		} else {
			$response = array(
				'ok'	=>	false,
				'msg'	=>	'<p>Passwords must be 12-64 characters, contain capital, '.
							'lowercase, 0-9 and one special character (!@#$%^*).</p>');
			return $response;
		}
	}
/***************************************************************************************************************************/

//	Function to validate new password - for use with auto generator
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function validate_a_pass ($new_pass) {
		if ( preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[0-9A-Za-z!@#$%^*]{12,64}$/', $new_pass) ) {
			$response = true;
		} else {
			$response = false;	
		}
		return $response;
	}
/***************************************************************************************************************************/

//	Function to change a user's password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function change_password ( array $data ) {
		$data	= (object) $data;
		//$hash	= sha1($_SESSION['prv_key'].$data->pub_key);
		//if ( $hash == $_SESSION['hash'] ) {
			global $user_ID;
			$userdata = array(
				'ID'			=>	$user_ID,
				'user_pass'		=>	$data->new_pass1
			);
			
			$rid = wp_update_user( $userdata );
			
			if ($rid == $user_ID) {
				$response = array(
					'ok'	=> true,
					'msg'	=> 'Your password was changed successfully!');
				return $response;
			} else {
				$crt_link = "<a href='mailto:crt@lmutual.com'>Customer Relations</a>";
				$response = array( 'ok' => false,
					'msg'	=>	"There was a problem updating your password. Please contact $crt_link.<br>" );
				return $response;
			}
		/*} else {
			$sh = $_SESSION['hash'];
			$yh = $hash;
			$spk = $_SESSION['pub_key'];
			$ypk = $data->pub_key;
			$crt_link = "<a href='mailto:crt@lmutual.com'>Customer Relations</a>";
			$response = array( 'ok' => false,
				'msg'	=>	"Unauthorized API transaction detected. Please contact $crt_link.<br>".
							"Session Hash: $sh<br>".
							"Your Hash: $yh<br>".
							"Session Public Key: $spk<br>".
							"Your Public Key: $ypk<br>" );
			return $response;
		}*/
	}
/***************************************************************************************************************************/

//	Function to geerate a secure password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sp_generate() {
		while (!validate_a_pass($spw)) {
			$spw = wp_generate_password(16, true, false);
		}
		$response = array(
			'ok'	=>	true,
			'msg'	=>	'<p>Secure password generated!</p>',
			'spw'	=>	$spw );
		return $response;
	}
/***************************************************************************************************************************/


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
/*	In the section of code below reside the functions which DO return data in JASON format.
/*	These functions will return data in the form of an array for use by other functions or conditional statements.
/*	
/***************************************************************************************************************************/


//	Run email validation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['check_email'] && $_POST['ajax'] == 'yes' ) {
    	echo json_encode(lm_check_email($_POST['check_email']));
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/

//	When the "regsubmit" post is sent, register a new BTC Mutual user account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['regsubmit'] && $_POST['ajax'] == 'yes' ) {
		$response = json_encode(lm_register_user($_POST));
		echo $response;
		exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/

//	When the "verify" post is sent, register a new BTC Mutual user account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['verify'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(activate_user($_POST['code'], $_POST['user'], $_POST['extra']));
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/

//	When the "verify_pass" post is sent, verify the users current password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['verify_pass'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(verify_current_password($_POST['verify_pass']));
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/

//	When the "validate_pass" post is sent, validate the new password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['validate_pass'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(validate_new_pass($_POST['validate_pass']));
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/

//	When the "new_pass1" post is sent, change the user's password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['new_pass1'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(change_password($_POST));
    	exit;	// only print out the json version of the response		
	}
/***************************************************************************************************************************/

//	When the "sp_generate" post is sent, generate the user's password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['sp_generate'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(sp_generate());
    	exit;	// only print out the json version of the response		
	}
/***************************************************************************************************************************/



//	Blank
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/***************************************************************************************************************************/

//	FOR DEBUGGING ONLY!
//	Comment out this entire section before uploading to production server.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
/***************************************************************************************************************************/

?>