<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		api-functions.php
//	Path:		/api/local/includes/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Returns the Base URL
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	function base_url(){
//		$protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
//		return $protocol . "://" . $_SERVER['HTTP_HOST'];
//	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	File Functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function create_folder( $path, $chmod = 755, $R = false ) {
		if ( !is_dir($path) ) {
			mkdir( $path, $chmod, $R );
		}
	}

	function write_file( $path, $filename, $mode = "r+", $write_data ) {
		$handle = fopen( $path.$filename, $mode );
		fwrite( $handle, $write_data );
		fclose( $handle );
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Generates a random alpha-numeric string of specified $length ($length defaults to 17)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function gen_rand_str( $length = 17 ) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Function for handeling error logging
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function raise_error($message) {
		// global $system_operator_mail, $system_from_mail;
		// date_default_timezone_set($timezone);											// Set the user's timezone
		$serror=
		"Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
		"Timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
		"Script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
		"Error:     " . $message ."\r\n\r\n";
		$date = date('Ymd');
		// Open a log file and write the error
		$fhandle = fopen( ABSPATH.'logs/errors-'.$date.'.log', 'a' );
		if($fhandle){
			$fwr = fwrite( $fhandle, $serror );
			fclose(( $fhandle ));
		}
		//if (error == $uid ) { $response = array( 'ok' => false, 'msg' => 'NOT fhandle!' ); return $response; }
		// E-mail error to system operator
		// if(!$b_debugmode)
		//   mail($system_operator_mail, 'error: '.$message, $serror, 'From: ' . $system_from_mail );
	}
/***************************************************************************************************************************/

//	Function for handeling event logging
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_event($message) {
		// global $system_operator_mail, $system_from_mail;
		// date_default_timezone_set($timezone);											// Set the user's timezone
		$sevent=
		"Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
		"Timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
		"Script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
		"Event:     " . $message ."\r\n\r\n";
		$date = date('Ymd');
		// Open a log file and write the error
		$fhandle = fopen( ABSPATH.'logs/events-'.$date.'.log', 'a' );
		if($fhandle){
			$fwr = fwrite( $fhandle, $sevent );
			fclose(( $fhandle ));
		}
		//if (error == $uid ) { $response = array( 'ok' => false, 'msg' => 'NOT fhandle!' ); return $response; }
		// E-mail error to system operator
		// if(!$b_debugmode)
		//   mail($system_operator_mail, 'error: '.$message, $serror, 'From: ' . $system_from_mail );
	}
/***************************************************************************************************************************/







//	Function that checks whether the given username is taken.
//	Since 0.0.1
//	Param string $username Username.
//	Return boolean True on success, and False on failure.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function username_taken( $username ) {
		$sfdb = sfdb();
		$dbq = $sfdb->prepare( "SELECT * FROM ".TP."_user_meta WHERE meta_key='username' AND meta_content = '".$username."'" );
		$dbq->execute();
		$row_count = $dbq->rowCount();
		//echo $row_count;
		if ( $row_count != 0 ) {
			return true;
		} elseif ( $row_count == 0 ) {
			return false;
		}
	}
/***************************************************************************************************************************/

//	Function for checking a username is valid and available
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_check_username( $username = '' ) {
		/*$response = array(
				'ok'	=> true, 
				'msg'	=> "Axax is working.");
		
		return $response;*/
		
		//$username = trim($username); // strip any white space
		$response = array(); // our response
		// if the username is blank
		if ( $username =='' ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Empty");
		// if the username does not match a-z or '.', '-', '_' then it's not valid
		} else if ( !preg_match( '/^[a-z0-9.-_]+$/', $username ) ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Only use a-Z, 0-9, or .-_");
		// this would live in an external library just to check if the username is taken
		} else if ( username_taken( $username ) ) {
			$response = array(
				'ok'	=> false, 
				'msg'	=> "Taken");
		// it's all good
		} else {
			$response = array(
				'ok'	=> true, 
				'msg'	=> "Available");
		}
		return $response;
	}
/***************************************************************************************************************************/

//	Run username validation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ( $_REQUEST['check_username'] && $_REQUEST['ajax'] == 'yes' ) {
		echo json_encode( sf_check_username( $_REQUEST['check_username'] ) );
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/





//	Function that checks whether the given email is valid.
//	Since 0.0.1
//	Param string $email Email.
//	Return boolean True on success, and False on failure.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function is_email( $email ) {
		if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
			return true;
		}
		else {
			return false;
		}
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Function that checks whether the given email is being used.
//	Since 0.0.1
//	Param string $username Username.
//	Return boolean True on success, and False on failure.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function email_exists( $email ) {
		$sfdb = sfdb();
		$dbq = $sfdb->prepare( "SELECT * FROM ".TP."_users WHERE email = '".$email."'" );
		$dbq->execute();
		//$dbq->fetch( PDO::FETCH_ASSOC );
		$row_count = $dbq->rowCount();
		if ( $row_count != 0 ) {
			return true;
		} elseif ( $row_count == 0 ) {
			return false;
		}
	}
/***************************************************************************************************************************/

//	Function for checking an email is valid and unused
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function lm_check_email( $email ) {
	if ( !is_email( $email ) ) {
		$response = array(
			'ok'	=> false, 
			'msg'	=> 'Invalid email address');
	} else if ( is_email( $email ) ) {
		if ( email_exists( $email ) ) {
			$response = array(
				'ok'	=> false,
				'msg'	=> 'Only one account per email is allowed');
		} else if ( !email_exists( $email ) ) {
			$response = array(
				'ok'	=> true,
				'msg'	=> 'Validated: '.$email );
		}
	}
	return $response;
}
/***************************************************************************************************************************/





//	Functions that register the user and send an email verification link
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sf_send_email_verification_link( $email, $link_code ) {
		$verification_link = "<a href='".BASE_URL."verify/?email=".$email."&verification_code=".$link_code."'>Verify Me!</a>";
		// Email the user a verification link
		$headers	=	'From: '.SITENAME.' Activations support@'.NAKED_SITE_URL."\r\n";
		$headers	.=	'Reply-To: no-reply@'.NAKED_SITE_URL."\r\n";
		$headers	.=	"Content-type: text/html\r\n";
		$subject	=	'Your New '.SITENAME.' Account - Please verify your email address.';
		$message	=	"<p><a href='".BASE_URL."'><img src='".BASE_URL."assets/logo/png/".SMALL_SITE_LOGO."'></a></p>".
						"<p>Hello,</p>".													// Activation email message
						"<p>To verify your email address please click the link below.</p>".
						"<p>$verification_link</p>".										// Verification link
						"<p>Thank you,<br>".												// Email closing
						SITENAME.' Support Team'."</p>";										// Signature
		mail( $email, $subject, $message, $headers );										// Send the verification email
	}

	function sf_register_user($post) {
		$sfdb = sfdb();
		$post = (object) $post;
		if ( $post->username != '' )			{ $un = true; }		else { $un = false; }
		if ( $post->email != '' ) 				{ $em = true; }		else { $em = false; }
		if ( $post->email2 != '' ) 				{ $em2 = true; }	else { $em2 = false; }
		if ( $post->email == $post->email2 ) 	{ $emm = true; }	else { $emm = false; }
		if ( strlen( $post->password ) > 7 ) 	{ $pw = true; }		else { $pw = false; }
		if ( $post->fname != '' )				{ $fn = true; }		else { $fn = false; }
		if ( $post->lname != '' )				{ $ln = true; }		else { $ln = false; }
		if ( $post->byear != '' )				{ $by = true; }		else { $by = false; }
		if ( $post->bmonth != '' )				{ $bm = true; }		else { $bm= false; }
		if ( $post->bday != '' ) 				{ $bd = true; }		else { $bd = false; }
		if ( $post->gender != '' )				{ $gd = true; }		else { $gd = false; }
		if ( $post->ajax == 'yes' )				{ $aj = true; }		else { $aj = false; }
		
		$email =		$post->email;
		$password = 	$post->password;
		
		$user_array = array (
			':email'		=> pg_escape_string( $email ),
			':password'		=> sha1( md5( pg_escape_string( $password ) ) ),
			);
			
		$link_code = md5( sha1( $email.$post->username.$password ) );
		
		$user_meta_array = array(
			'username'	=> $post->username,
			'fname'		=> $post->fname,
			'lname'		=> $post->lname,
			'bday'		=> $post->bday,
			'bmonth'	=> $post->bmonth,
			'byear'		=> $post->byear,
			'gender'	=> $post->gender,
			'activated' => '0',
			'activation_code' => $link_code );
		
		if ($aj) {
			$i = $sfdb->add_user( $user_array, $user_meta_array, 1 );			
			if ( $i['ok'] )	{
				$response = array(
					'ok'		=>	true,
					'msg'		=>	'Thank you for signing up at Lurkable,'.$post->fname.'.<br>'.
									'An email verification link has been sent to '.
									$email.'.<br>'.'Please check your email to complete the signup.' );
				sf_send_email_verification_link( $email , $link_code );
			} elseif ( !$i['ok'] ) {
				raise_error( $i['msg'] );
				$response = array(
					'ok'		=>	false,
					'msg'		=>	"An internal error has occured. It's not your fault. The error was reported!<br>" );
			}
		} elseif ( !$aj ) {
			if ($un && $em && $em2 && $emm && $pw && $fn && $ln && $by && $bm && $bd && $gd) {
				$i = $sfdb->add_user( $user_array, $user_meta_array, 0 );
				if ( $i['ok'] ) {
					$response = array(
						'ok'		=>	true,
						'msg'		=>	'Thank you for signing up at Lurkable.<br>'.
										'An email verification link has been sent to '.
										$post->email.'.<br>'.'Please check your email to complete the signup.' );
					sf_send_email_verification_link( $email , $link_code );
				} elseif ( !$i['ok'] ) {
					raise_error( $i['msg'] );
					$response = array(
						'ok'		=>	false,
						'msg'		=>	"An internal error has occured. It's not your fault. The error was reported!<br>" );
				}
			} elseif ( !$un ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please enter your desired username.<br>' );
					return $response;
			} elseif ( !$em ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your email.<br>' );
					return $response;
			} elseif ( !$em2 ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must re-enter your email.<br>' );
					return $response;
			} elseif ( !$emm ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'The email fields must match exactly.<br>' );
					return $response;
			} elseif ( !$pw ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Password must be 8+ characters using a-Z, 0-9, [.-_].<br>' );
					return $response;
			} elseif ( !$fn ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please provide your real first name.<br>' );
					return $response;
			} elseif ( !$ln ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please provide your real last name.<br>' );
					return $response;
			} elseif ( !$by ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth year.<br>' );
					return $response;
			} elseif ( !$bm ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth month.<br>' );
					return $response;
			} elseif ( !$bd ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'You must provide your birth day.<br>' );
					return $response;
			} elseif ( !$gd ) {
				$response = array(
					'ok'		=>	false,
					'msg'		=>	'Please select your gender.<br>' );
					return $response;
			}
		}
		return $response;	
	}

	// When the "regsubmit" post is sent, register a new BTC Mutual user account
	if ($_POST['regsubmit'] && $_POST['ajax'] == 'yes' ) {
		$response = json_encode(sf_register_user($_POST));
		echo $response;
		exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/



//	Activate a useres login account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function activate_user($code = '', $user = '', $extra = '') {
		$sfdb = sfdb();
		if ($code != '' && $user != '' && $extra != '') {
			$fusername			= $user.'_'.$code;
			$userdata			= get_user_by('login', $fusername);
			$uid				= $userdata->ID; // the id of the user
			$email				= $userdata->user_email;
			$password			= wp_generate_password(16, false, false);
			$login_link			= "<a href='http://lmutual.com/login'>Login Here</a>";
			$change_pass_link	= "<a href='http://lmutual.com/change-password'>Click Here</a>";
			$userdata2 = array(
				'user_login'	=>	$user, );
			$userdata3 = array(
				'ID'			=>	$uid,
				'user_pass'		=>	$password,
				'user_nicename' =>	$user,
				'display_name'	=>	$user );
			$wpdb->update($wpdb->users, $userdata2, array('ID' => $uid));
			$rid = wp_update_user( $userdata3 );
			// Email the user for verification
			$headers	=	'From: Lurkable.me Activations <support@lurkable.me>'."\r\n";
			$subject	=	'Your New Lurkable Account - Your account has been activated.';
			$message	=	"<p>Hello,</p>".													// Activation email message
							"<p>Your email has been verified and your login has been activated.</p>".
							"<p>You can login and setup your account using these credentials.</p>".
							"<p>Username: ".$user."<br>".										// The login username
							'Password: '.$password."</p>".										// The user's password
							"<p>".$login_link."</p>".
							"<p>You can always change your password under account settings.</p>".
							"<p>".$change_pass_link."</p>".
							"<p>Happy lurking,<br>".											// Email closing
							'Lurkable Support Team'."</p>";										// Signature
			mail( $email, $subject, $message, $headers );										// Send the verification email
			$creds = array(
				'user_login'	=> $user,
				'user_password'	=> $password,
				'remember'		=> true );
			if ( $rid == $uid) { $user_logged = wp_signon( $creds, true ); }
			if (is_wp_error($user_logged)) { $login_error = $user_logged->get_error_message();
				$errorM	=	'There was an error automatically logging in the user, '.
							$user.', while completing the email verification and user activation process. - Error: '.
							$login_error;
				raise_error ($errorM);
				$response	= array (
					'ok'	=> false,
					'msg'	=> 'There was an error automatically logging you in.' );
			} else {
				$response	=	array (
					'ok'	=>	true,
					'msg'	=>	'Your email has been verified.'."<br>".
								'You have been automatically logged in.'."<br>".
								'A temporary password has been emailed to: '.$userdata->user_email."<br>".
								'However, you should change your password to something memorable.'."<br>".
								'If you are not automatically redirected in a few seconds '.$change_pass_link.'.' );
			}
		} else {
			$response	= array (
				'ok'	=> false,
				'msg'	=> 'Invalid request.' );
		}
		return $response;
	}
/***************************************************************************************************************************/

//	Function to validate new password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function validate_new_pass ($new_pass) {
		if ( preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[0-9A-Za-z!@#$%^*]{12,64}$/', $new_pass) ) {
			$response = array(
				'ok'	=> true,
				'msg'	=> '<p>Congratulations your password is strong!</p>');
			return $response;
		} else {
			$response = array(
				'ok'	=>	false,
				'msg'	=>	'<p>Passwords must be 12-64 characters, contain capital, '.
							'lowercase, 0-9 and one special character (!@#$%^*).</p>');
			return $response;
		}
	}
/***************************************************************************************************************************/

//	Function to validate new password - for use with auto generator
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function validate_a_pass ($new_pass) {
		if ( preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*])[0-9A-Za-z!@#$%^*]{12,64}$/', $new_pass) ) {
			$response = true;
		} else {
			$response = false;	
		}
		return $response;
	}
/***************************************************************************************************************************/

//	Function to change a user's password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function change_password ( array $data ) {
		$data	= (object) $data;
		//$hash	= sha1($_SESSION['prv_key'].$data->pub_key);
		//if ( $hash == $_SESSION['hash'] ) {
			global $user_ID;
			$userdata = array(
				'ID'			=>	$user_ID,
				'user_pass'		=>	$data->new_pass1
			);
			
			$rid = wp_update_user( $userdata );
			
			if ($rid == $user_ID) {
				$response = array(
					'ok'	=> true,
					'msg'	=> 'Your password was changed successfully!');
				return $response;
			} else {
				$crt_link = "<a href='mailto:crt@lmutual.com'>Customer Relations</a>";
				$response = array( 'ok' => false,
					'msg'	=>	"There was a problem updating your password. Please contact $crt_link.<br>" );
				return $response;
			}
		/*} else {
			$sh = $_SESSION['hash'];
			$yh = $hash;
			$spk = $_SESSION['pub_key'];
			$ypk = $data->pub_key;
			$crt_link = "<a href='mailto:crt@lmutual.com'>Customer Relations</a>";
			$response = array( 'ok' => false,
				'msg'	=>	"Unauthorized API transaction detected. Please contact $crt_link.<br>".
							"Session Hash: $sh<br>".
							"Your Hash: $yh<br>".
							"Session Public Key: $spk<br>".
							"Your Public Key: $ypk<br>" );
			return $response;
		}*/
	}
/***************************************************************************************************************************/

//	Function to geerate a secure password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function sp_generate() {
		while (!validate_a_pass($spw)) {
			$spw = wp_generate_password(16, true, false);
		}
		$response = array(
			'ok'	=>	true,
			'msg'	=>	'<p>Secure password generated!</p>',
			'spw'	=>	$spw );
		return $response;
	}
/***************************************************************************************************************************/


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
/*	In the section of code below reside the functions which DO return data in JASON format.
/*	These functions will return data in the form of an array for use by other functions or conditional statements.
/*	
/***************************************************************************************************************************/


//	Run email validation
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['check_email'] && $_POST['ajax'] == 'yes' ) {
    	echo json_encode(lm_check_email($_POST['check_email']));
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/


//	When the "verify" post is sent, register a new BTC Mutual user account
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['verify'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(activate_user($_POST['code'], $_POST['user'], $_POST['extra']));
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/

//	When the "verify_pass" post is sent, verify the users current password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['verify_pass'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(verify_current_password($_POST['verify_pass']));
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/

//	When the "validate_pass" post is sent, validate the new password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['validate_pass'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(validate_new_pass($_POST['validate_pass']));
    	exit;	// only print out the json version of the response
	}
/***************************************************************************************************************************/

//	When the "new_pass1" post is sent, change the user's password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['new_pass1'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(change_password($_POST));
    	exit;	// only print out the json version of the response		
	}
/***************************************************************************************************************************/

//	When the "sp_generate" post is sent, generate the user's password
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ($_POST['sp_generate'] && $_POST['ajax'] == 'yes' ) {
		echo json_encode(sp_generate());
    	exit;	// only print out the json version of the response		
	}
/***************************************************************************************************************************/



	function set_avatar( $url ) {
		$uid = $_COOKIE[SITENAME.'_uid'];
		$sfdb = sfdb();
		$response = $sfdb->set_avatar( $uid, $url );
		return $response;
	}

		if ( $_POST['set_avatar'] ) {
			$response = set_avatar( $_POST['url'] );
			echo json_encode( $response );
		}



//	Blank
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/***************************************************************************************************************************/

//	FOR DEBUGGING ONLY!
//	Comment out this entire section before uploading to production server.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
/***************************************************************************************************************************/

?>