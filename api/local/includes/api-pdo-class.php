<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		pdo-class.php
//	Path:		/themes/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Create the PDO extension class SFDB
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SFDB extends PDO {
	
	function update( $table, array $what, array $where ) {			// Declair the SFDB->update function
		foreach ($where as $key => $value) {						// Iterate through the $where array
			if (is_integer($value)) {								// Catch integers
				$conditions .= "$key = $Value AND ";				// Do not apply single quotes to integers 
			} else {												// Catch strings
				$conditions .= "$key = '".$Value."' AND ";			// Apply single quotes to strings
			}	
		}
		$conditions = rtrim( $conditions, " AND ");					// Trim the extra " AND " from the right of $conditions
		foreach ($what as $key => $value) {							// Iterate through the $what array
			if (is_numeric($value)) {								// Catch integers
				$columns .= "$key = $value, ";						// Do not apply single quotes to integers 
			} else {												// Catch strings
				$columns .= "$key = '".$value."', ";				// Apply single quotes to strings
			}
		}
		$columns = rtrim($columns, ", ");							// Trim the extra ", " from the right of $columns
		$dbquery = "UPDATE $table SET $columns $conditions";		// Define the SQL query
		$this->beginTransaction();									// Begin the transaction
		$dbquery = $this->prepare($dbquery);						// Prepare the SQL query
		$this->exec($dbquery);										// Execture the SQL query
		$wahappen = "Executed SQL: ".$dbquery;
	}
	
	function insert( $table, array $what ) {	// Declair the SFDB->insert function
		foreach ($what as $key => $value) {		// Iterate throught the $what array
			$keys .= "$key, ";					// Build the $keys string for the SQL statement
			//if ($key = 'account_num') { $value = (int) $value; }
			if (is_numeric($value)) {			// Catch integers
				$values .= "$value, ";			// Do not apply single quotes to integers
			} else {							// Catch strings
				$values .= "'".$value."', ";	// Apply single quotes to strings
			}
		}
		$keys	= rtrim($keys, ", ");								// Trim the extra ", " from the right of $keys
		//echo '('.$keys.')'; echo '('.$values.')';
		$values	= rtrim($values, ", ");								// Trim the extra ", " from the right of $values
		$dbquery = "INSERT INTO $table ($keys) VALUES ($values)";	// Define the SQL query
		//echo $dbquery;
		//$this->beginTransaction();								// Begin the transaction
		//$dbquery = $this->prepare($dbquery);						// Prepare the SQL query
		$this->exec($dbquery);										// Execture the SQL query
		$wahappen = "Executed SQL: ".$dbquery;
	}
	
	function delete( $table, array $where ) {					// Declair the SFDB->delete function
		foreach ( $where as $key => $value ) {					// Iterate throught the $where array
			if (is_numeric($value)) {							// Catch integers
				$conditions .= "$key = $Value AND ";			// Do not apply single quotes to integers
			} else {											// Catch strings
				$conditions .= "$key = '".$Value."' AND ";		// Apply single quotes to strings
			}
		}
		$conditions = rtrim( $conditions, " AND ");				// Trim the extra " AND " from the right of $conditions
		$dbquery = "DELETE FROM $table WHERE $conditions";		// Define the SQL query
		$this->beginTransaction();								// Begin the transaction
		$dbquery = $this->prepare($dbquery);					// Prepare the SQL query
		$this->exec($dbquery);									// Execture the SQL query
		$wahappen = "Executed SQL: ".$dbquery;
	}
	
	function select_all( $table, array $where, $order = '' ) {	// Declair the SFDB->select_all function
		foreach ( $where as $key => $value ) {					// Iterate throught the $where array
			if ( is_numeric($value) ) {							// Catch integers
				$conditions .= "$key = $value AND ";			// Do not apply single quotes to integers
			} else {											// Catch strings
				$conditions .= "$key = '".$value."' AND ";		// Apply single quotes to strings
			}
		}
		$conditions = rtrim( $conditions, " AND ");				// Trim the extra " AND " from the right of $conditions
		if ( $order != '' ) {									// Catch $order with value
			$order = " ORDER BY ".$order;						// Finish constructing the $order string
		}
		$dbquery = "SELECT * FROM $table WHERE $conditions".$order;	// Define the SQL query
		//echo $dbquery;
		//$this->beginTransaction();							// Begin the transaction
		$prepared = $this->prepare($dbquery);					// Prepare the SQL query
		return $prepared;
	}
	
	function select_col( $what, $table, array $where, $order = '' ) {	// Declair the SFDB->select_col function
		foreach ( $where as $key => $value ) {					// Iterate throught the $where array
			if ( is_numeric($value) ) {							// Catch integers
				$conditions .= "$key = $value AND ";			// Do not apply single quotes to integers
			} else {											// Catch strings
				$conditions .= "$key = '".$value."' AND ";		// Apply single quotes to strings
			}
		}
		$conditions = rtrim( $conditions, " AND ");				// Trim the extra " AND " from the right of $conditions
		if ( $order != '' ) {									// Catch $order with value
			$order = " ORDER BY ".$order;						// Finish constructing the $order string
		}
		$dbquery = "SELECT ".$what." FROM $table WHERE $conditions".$order;	// Define the SQL query
		//echo $dbquery;
		//$this->beginTransaction();							// Begin the transaction
		$prepared = $this->prepare($dbquery);					// Prepare the SQL query
		return $prepared;
	}
	
	function select_cols( $what, $table, array $where, $order = '' ) {	// Declair the SFDB-select function
		foreach ( $where as $key => $value ) {					// Iterate throught the $where array
			if ( is_numeric($value) ) {							// Catch integers
				$conditions .= "$key = $value AND ";			// Do not apply single quotes to integers
			} else {											// Catch strings
				$conditions .= "$key = '".$value."' AND ";		// Apply single quotes to strings
			}
		}
		$conditions = rtrim( $conditions, " AND ");				// Trim the extra " AND " from the right of $conditions
		if ( $order != '' ) {									// Catch $order with value
			$order = " ORDER BY ".$order;						// Finish constructing the $order string
		}
		$dbquery = "SELECT ".$what." FROM $table WHERE $conditions".$order;	// Define the SQL query
		//echo $dbquery;
		//$this->beginTransaction();							// Begin the transaction
		$prepared = $this->prepare($dbquery);					// Prepare the SQL query
		return $prepared;
	}
	
	
	
	
	function add_user( array $user_array, array $user_meta_array ) {
		$prepared = $this->prepare("INSERT INTO ".TP."_users ( email, password ) VALUES ( :email, :password )");
		try {
			$this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$this->beginTransaction();
			$prepared->execute( $user_array );
			global $new_user_id;
			$new_user_id = $this->lastInsertId('users_id_seq');
			foreach ( $user_meta_array as $meta_key => $meta_content ) {
				$values = "$new_user_id, 1, :meta_key, :meta_content";
				$token_values = array(
					":meta_key"		=> $meta_key, 
					":meta_content"	=> $meta_content
					);
				$prepared = $this->prepare( "INSERT INTO ".TP."_user_meta ( user_id, group_id, meta_key, meta_content) VALUES ( $values )" );
				$prepared->execute( $token_values );
			}
		} catch ( Exception $e ) {
			$db_err = $e->getMessage();
			$this->rollBack();
			$response = array(
				'ok'	=>	false,
				'msg'	=>	$db_err );
			return $response;
		}
		$this->commit();
		$response = array(
			'ok'	=>	true,
			'msg'	=>	"Success!" );
		return $response;
	}
	
	
	
	
	function add_user_media( $uid, array $user_media ) {	
		foreach ( $user_media as $filename => $details ) {
			$prepared = $this->prepare( "INSERT INTO ".TP."_user_media (user_id, media_type, filename, path, url) VALUES (:uid, :media_type, :filename, :path, :url)" );
			$media_array = array(
				':uid' => $uid,
				':media_type' => $details['type'],
				':filename' => $filename,
				':path' => $details['path'],
				':url' => $details['url'] );
			try {
				$prepared->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				$prepared->beginTransaction();
				$prepared->execute( $media_array );
			} catch ( Exception $e ) {
				$db_err = $e->getMessage();
				$this->rollBack();
				$response = array(
					'ok'	=>	false,
					'msg'	=>	$db_err );
				return $response;
			}
			$response = array(
				'ok'	=>	true,
				'msg'	=>	"Success!" );
			return $response;
		}
	}
	
	function update_user_media( array $user_media ) {
		foreach ( $user_media as $filename => $details ) {
			$prepared = $this->prepare( "UPDATE ".TP."_user_media SET media_type=:media_type, filename=:filename, path=:path, url=:url WHERE id=:media_id" );
			$media_array = array(
				':media_type' => $details['type'],
				':filename' => $filename,
				':path' => $details['path'],
				':url' => $details['url'],
				':media_id' => $details['media_id'] );
			try {
				$prepared->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
				$prepared->beginTransaction();
				$prepared->execute( $media_array );
			} catch ( Exception $e ) {
				$db_err = $e->getMessage();
				$this->rollBack();
				$response = array(
					'ok'	=>	false,
					'msg'	=>	$db_err );
				return $response;
			}
			$response = array(
				'ok'	=>	true,
				'msg'	=>	"Success!" );
			return $response;
		}
	}
	
	
	
	
	
	function delete_old_local_api_tokens() {	
		$this->exec( "DELETE FROM ".TP."_local_api_tokens WHERE api_hits > 15" );
		$this->exec( "DELETE FROM ".TP."_local_api_tokens WHERE created < (now() - interval '10' minute)" );
	}
	
	function verify_local_api_token( $api_token ) {
		$dbquery = $this->prepare( "SELECT * FROM ".TP."_local_api_tokens WHERE token = '".$api_token."'" );
		$dbquery->execute();
		$row_count = $dbquery->rowCount();
		//echo $row_count;
		if ( $row_count == 1 ) {
			$response = array(
						'ok'	=> true,
						'msg'	=> 'This session is valid.' );
			return $response;
		} else {
			$response = array(
						'ok'	=> false,
						'msg'	=> 'This session has expired.' );
			return $response;
		}
	}
	
	function update_local_api_token( $api_token ) {
		$dbq = $this->prepare( "SELECT * FROM ".TP."_local_api_tokens WHERE token = '".$api_token."'" );
		$dbq->execute();
		$results = $dbq->fetch( PDO::FETCH_ASSOC );
		$api_hits = $results['api_hits'];
		$api_hits++;
		
		$dbq = $this->prepare( "UPDATE ".TP."_local_api_tokens SET api_hits = ".$api_hits." WHERE token = '".$api_token."'" );
		$dbq->execute();
	}
	
	
	
	
	
	function login_token( $last_email ) {
		$dbquery = "SELECT token FROM auto_login WHERE email = ".$last_email;
		$prepared = $this->prepare($dbquery);					// Prepare the SQL query
		return $prepared;
	}
	
	function user_id( $email ) {
		$dbquery = "SELECT id FROM users WHERE email = ".$email;
		$prepared = $this->prepare($dbquery);					// Prepare the SQL query
		return $prepared;
	}
	
	function username_taken($username) {
		$dbquery = $this->prepare("SELECT id FROM sf_user_meta WHERE name='username' value= '$username'");
		$dbquery->exec($dbquery);
		$result = $prepared->fetchColumn(0);
		return $return;
	}
	
	
	
	
	
	// This may or may not be used in future updates
	function general_settings() {
		$prepared = $this->prepare( "SELECT * FROM ".TP."_settings WHERE type = 'general'" );
		try {
			$this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$this->beginTransaction();
			$prepared->execute();
		} catch ( PDOException $e ) {
			$db_err = $e->getMessage();
			$this->rollBack();
			$response = array(
				'ok'	=>	false,
				'msg'	=>	$db_err." ".$prepared->queryString );
			return $response;
		}
		$this->commit();
		$results = $prepared->fetchAll( PDO::FETCH_ASSOC );
		foreach ( $results as $setting ) {
			$settings[$setting['name']] = $setting['value'];
		}
		return (object) $settings;
	}





	function set_avatar( $uid, $url ) {
		$bound_parameters = array(
			':uid' => $uid,
			':url' => $url );
		$prepared = $this->prepare( "UPDATE ".TP."_user_avatars SET url=:url WHERE user_id=:uid" );
		try {
			$this->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$this->beginTransaction();
			$prepared->execute( $bound_parameters );
		} catch ( PDOException $e ) {
			$db_err = $e->getMessage();
			$this->rollBack();
			$response = array(
				'ok'	=>	false,
				'msg'	=>	$db_err );
			return $response;
		}
		$this->commit();
		$response = array(
			'ok'	=>	true,
			'msg'	=>	'Success!' );
		return $response;
	}
	
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>