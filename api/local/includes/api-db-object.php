<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		db-object.php
//	Path:		/themes/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Create a new PDO database object
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class ConnectionFactory {
    private static $factory;
    public static function getFactory()
    {
        if (!self::$factory)
            self::$factory = new ConnectionFactory();
        return self::$factory;
    }

    private $db;

    public function getConnection() {
        if (!$db) {
			try {
				$db = new SFDB( DB_TYPE.":host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME, DB_USER, DB_PASS,
					array( PDO::ATTR_PERSISTENT => true ) );
				define( 'DB_CONNECTED', true );
			} catch (Exception $e) {
				define( 'DB_CONNECTED', false );
				$db_err = $e->getMessage();
				$e_code = $e->getCode();
				if ( $e_code == 2002 ) {
					$db_conn_err = "Error: [{$e_code}] Make sure the hostname specified for '$"."db_host' in ".ABSPATH.
						"config.php is correct.<br>For help refer to the SocialFlood setup manual.";
				} elseif ( $e_code == 0 ) {
					$db_conn_err = "Error: [{$e_code}] Make sure the database setting '$"."db_type' is correct in ".ABSPATH.
						"config.php is correct.<br>For help, refer to the SocialFlood setup manual.";
				} elseif ( $e_code() == 1044 ) {
					$db_conn_err = "Error: [{$e_code}] Make sure the database setting '$"."db_name' is correct in ".ABSPATH.
						"config.php.<br> For help, refer to the SocialFlood setup manual.";
				} elseif ( $e_code() == 1045 ) {
					$db_conn_err = "Error: [{$e_code}] Make sure the database settings '$"."db_user' and '$"."db_password' are correct in ".ABSPATH.
						"config.php.<br>For help, refer to the SocialFlood setup manual.";
				} else {
					$db_con_err = "Error: [{$e->getCode()}] $db_err";
				}
			}
		}
		if ( !DB_CONNECTED ) {
			return $db_conn_err;
		} elseif ( DB_CONNECTED ) {
			//echo "DB connected";
			return $db;
		}
    }
}

function sfdb()
{
    $conn = ConnectionFactory::getFactory()->getConnection();
	return $conn;
}

	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>