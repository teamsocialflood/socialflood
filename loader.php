<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		loader.php
//	Path:		/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Start the session
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	session_start();
/***************************************************************************************************************************/

//	Define Absolute & Core Paths
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Absolute & Core Paths
	define( 'ABSPATH', dirname(__FILE__) . '/' );
	define( 'CORE', ABSPATH . 'core/' );
/***************************************************************************************************************************/

//	Define Core Shared Resource Paths
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	define( 'CORE_SHARED', CORE . 'shared/' );
	define( 'CORE_SHARED_LIB', CORE_SHARED . 'lib/' );
	define( 'CORE_SHARED_MODELS', CORE_SHARED . 'models/' );
	define( 'CORE_SHARED_ROUTING', CORE_SHARED . 'routing/' );
	define( 'CORE_SHARED_LIB_PHP', CORE_SHARED_LIB . 'php/');
	define( 'CORE_SHARED_LIB_PHP_CLASSES', CORE_SHARED_LIB_PHP . 'classes/' );
	define( 'CORE_SHARED_LIB_JS', CORE_SHARED_LIB . 'js/' );
/***************************************************************************************************************************/

//	Define Theme & Plugins Base Paths
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	define( 'THEMES_BASE', ABSPATH . 'themes/' );
	define( 'PLUGINS_BASE', ABSPATH . 'plugins/' );
/***************************************************************************************************************************/

//	Loads the configuration file(s)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function load_conf_file() {
			require_once( ABSPATH.'config.php' );
	}
/***************************************************************************************************************************/
	
//	Loads Core Shared Resources
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function load_shared_classes() {
		require_once( CORE_SHARED_LIB_PHP . 'sf-core-shared-class-loader.php' );
	}
/***************************************************************************************************************************/

//	Loads the general setting from the database
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function load_general_settings() {
		global $general_settings;
		$sfdb = sfdb();
		$general_settings = $sfdb->general_settings();
		// Define the SITENAME & TAGLINE
		define( 'SITENAME', $general_settings->sitename );
		define( 'TAGLINE', $general_settings->tagline );
		define( 'FRONT_PAGE_FEATURED_IMAGE', $general_settings->front_page_featured_img_url );
	}
/***************************************************************************************************************************/

//	Loads the Core Shared Functions
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function load_core_shared_functions() {
		// Include the Core Functions
		require_once( CORE_SHARED_LIB_PHP . 'sf-core-shared-functions.php' );
	}
/***************************************************************************************************************************/

//	Define Session Required Core Resource Paths
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function define_required_core_resource_paths() {
		// Core Admin Resource Paths
		if ( sf_is_user_admin() ) { 
			define( 'CORE_ADMIN', CORE . 'admin/' );
			define( 'CORE_ADMIN_LIB', CORE_ADMIN . 'lib/' );
			define( 'CORE_ADMIN_MODELS', CORE_ADMIN . 'models/' );
			define( 'CORE_ADMIN_ROUTING', CORE_ADMIN . 'routing/' );
			define( 'CORE_ADMIN_LIB_PHP', CORE_ADMIN_LIB . 'php/' );
			define( 'CORE_ADMIN_LIB_PHP_CLASSES', CORE_ADMIN_LIB_PHP . 'classes/' );
			define( 'CORE_ADMIN_LIB_JS', CORE_ADMIN_LIB . 'js/' );
		}
	
		// Core Front-End Resource Paths
		if ( !sf_is_user_logged_in() ) {
			define( 'CORE_FRONT', CORE . 'front-end/' );
			define( 'CORE_FRONT_LIB', CORE_FRONT . 'lib/' );
			define( 'CORE_FRONT_MODELS', CORE_FRONT . 'models/' );
			define( 'CORE_FRONT_ROUTING', CORE_FRONT . 'routing/' );
			define( 'CORE_FRONT_LIB_PHP', CORE_FRONT_LIB . 'php/' );
			define( 'CORE_FRONT_LIB_PHP_CLASSES', CORE_FRONT_LIB_PHP . 'classes/' );
			define( 'CORE_FRONT_LIB_JS', CORE_FRONT_LIB . 'js/' );
			
		}
	
		// Core Back-End Resource Paths
		if ( sf_is_user_logged_in() ) {
			define( 'CORE_BACK', CORE . 'back-end/' );
			define( 'CORE_BACK_LIB', CORE_BACK . 'lib/' );
			define( 'CORE_BACK_MODELS', CORE_BACK . 'models/' );
			define( 'CORE_BACK_ROUTING', CORE_BACK . 'routing/' );
			define( 'CORE_BACK_LIB_PHP', CORE_BACK_LIB . 'php/' );
			define( 'CORE_BACK_LIB_PHP_CLASSES', CORE_BACK_LIB_PHP . 'classes/' );
			define( 'CORE_BACK_LIB_JS', CORE_BACK_LIB . 'js/' );
		}
	}
/***************************************************************************************************************************/

//	Define the Current Protocol & Protocols
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$protocol = ( isset( $_SERVER['HTTPS'] ) &&  strtolower( $_SERVER['HTTPS'] ) == 'on' ) ? "https" : "http";
	define( 'PROTOCOL', $protocol );
	define( 'NON_SECURE', 'http://' );
	define( 'SECURE', 'https://' );
/***************************************************************************************************************************/

//	Define Naked and Base URLs
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	define( 'NAKED_SITE_URL', $_SERVER['HTTP_HOST'] );
	define( 'BASE_URL', PROTOCOL . "://" . $_SERVER['HTTP_HOST'] . '/' );
	define( 'SECURE_BASE_URL',  SECURE . NAKED_SITE_URL . '/' );
	define( 'NON_SECURE_BASE_URL', NON_SECURE . NAKED_SITE_URL . '/' );
/***************************************************************************************************************************/

//	Define Session Required Core Resource URLs
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function define_required_core_resource_urls() {
		define( 'CORE_SHARED_LIB_JS_URL', BASE_URL . 'core/shared/lib/js/' );
		// Core Admin Resource Paths
		if ( sf_is_user_admin() ) { 
			define( 'CORE_ADMIN_LIB_JS_URL', BASE_URL . 'core/admin/lib/js/' );
			define( 'CORE_ADMIN_CSS_URL', BASE_URL . 'core/admin/assets/css/' );
		}
	
		// Core Front-End Resource Paths
		if ( !sf_is_user_logged_in() ) {
			define( 'CORE_FRONT_LIB_JS_URL', BASE_URL . 'core/front-end/lib/js/' );
			define( 'CORE_FRONT_CSS_URL', BASE_URL . 'core/front-end/assets/css/' );
		}
	
		// Core Back-End Resource Paths
		if ( sf_is_user_logged_in() ) {
			define( 'CORE_BACK_LIB_JS_URL', BASE_URL . 'core/back-end/lib/js/' );
			define( 'CORE_BACK_CSS_URL', BASE_URL . 'core/back-end/assets/css/' );
		}
	}
/***************************************************************************************************************************/

//	Define Core Shared Resource URLs
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	define( 'SF_LOGOUT_URL', SECURE_BASE_URL . 'logout/' );
	define( 'SF_LOGIN_URL', SECURE_BASE_URL );
/***************************************************************************************************************************/

//	Define security keys and salts
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	define('LOGGED_IN_KEY',    'eu7zT9OMr%+z/_Y45#g`-g]uinKX6vav5pZ  K|HxX4(;Q7CvXy|Xtv^-{gL+LBD');
	define('LOGGED_IN_SALT',   'Q},=dB<!PGz+h2N|2H*}}rA1fR5Bpg[~Pp$/6]XYikEj,_|dA@55nqSx+y$/3nr ');
	define('NONCE_KEY',        '%J81) (Sj]s[zn-P`JOtFLt4D,K4[$lA$:`OVR0X|w}+}adUV/)X!{_/7 ]LB)R_');
	define('NONCE_SALT',       'NiM96#5aS^yJru9]8emtD%~BFMcYR-#L0-@>XJiC/:bAlPd5^5(6diu3md`tG(P0');
	define('AUTH_KEY',         '4}kFWIe.,kd75B9&h-{u|~XdXEHy1X|rqznl.&?sHSX8<ya-:MFQa|DryK<X BYl');
	define('AUTH_SALT',        ',$4Ar-](^Z_Xg&5kP~?Thdwzlk#00l|N,dK?QCz6bx9Tx~_2bN(r--j^x`C5:f.g');
	define('SECURE_AUTH_KEY',  'mP.{A#YanxhPM2FQ| %H?1-`CRo+);p<c<+u*?62s_QS1txJVAtlsf`:OnsDjqwS>$O>v[OBpPzB4h.H7{[ir2{j+Kdn0[d5Agnt{Ig)#a%jBA7-1f*LHKNtYA|[]2WI');
	define('SECURE_AUTH_SALT', '-V7E(Hi4n=YKy5RF|[R6B4v7jbl%y67#6O g-UuUYkUp4/q|%pZvgW|ZgqtJtX#A?a*sZN4,Av^1?mA$|~#:4v1Qs^u0:S=9_l)}LxG- AyY<w3s%l.%9[A)?41-F4U5');
/***************************************************************************************************************************/

//	Loader Required Functions 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	//	Sets up the Theme
	function setup_theme() {
		global $theme_settings;
		$sfdb = sfdb();
		$theme_settings = $sfdb->theme_settings();
		$current_theme = $theme_settings->active;
		$current_theme_dir = $theme_settings->directory;
		
		// Define the current theme name
		define( 'CURRENT_THEME_NAME', $current_theme );
		
		// Define the current theme paths
		define( 'CURRENT_THEME_PATH', THEMES_BASE . $current_theme_dir . '/' );
		define( 'CURRENT_THEME_ADMIN_VIEWS_PATH', CURRENT_THEME_PATH . 'admin/views/' );
		define( 'CURRENT_THEME_BACK_VIEWS_PATH', CURRENT_THEME_PATH . 'back-end/views/' );
		define( 'CURRENT_THEME_FRONT_VIEWS_PATH', CURRENT_THEME_PATH . 'front-end/views/' );
		define( 'CURRENT_THEME_SHARED_VIEWS_PATH', CURRENT_THEME_PATH . 'shared/views/' );
		
		// Define the current theme Shared URLs
		define( 'CURRENT_THEME_URL', BASE_URL . 'themes/' . $current_theme_dir . '/' );
		define( 'CURRENT_THEME_SHARED_URL', CURRENT_THEME_URL . 'shared/' );
		define( 'CURRENT_THEME_SHARED_ASSETS_URL', CURRENT_THEME_SHARED_URL . 'assets/' );
		define( 'CURRENT_THEME_SHARED_CSS_URL', CURRENT_THEME_SHARED_ASSETS_URL . 'css/' );
		define( 'CURRENT_THEME_SHARED_FONTS_URL', CURRENT_THEME_SHARED_ASSETS_URL . 'fonts/' );
		define( 'CURRENT_THEME_SHARED_GIF_URL', CURRENT_THEME_SHARED_ASSETS_URL . 'gif/' );
		define( 'CURRENT_THEME_SHARED_ICO_URL', CURRENT_THEME_SHARED_ASSETS_URL . 'ico/' );
		define( 'CURRENT_THEME_SHARED_JPG_URL', CURRENT_THEME_SHARED_ASSETS_URL . 'jpg/' );
		define( 'CURRENT_THEME_SHARED_JS_URL', CURRENT_THEME_SHARED_URL . 'lib/js/' );
		define( 'CURRENT_THEME_SHARED_PNG_URL', CURRENT_THEME_SHARED_ASSETS_URL . 'png/' );
	}
	
	function define_required_theme_resource_urls() {
		if ( sf_is_user_admin() ) {
			define( 'CURRENT_THEME_ADMIN_URL', CURRENT_THEME_URL . 'admin/' );
			define( 'CURRENT_THEME_ADMIN_ASSETS_URL', CURRENT_THEME_ADMIN_URL . 'assets/' );
			define( 'CURRENT_THEME_ADMIN_CSS_URL', CURRENT_THEME_ADMIN_ASSETS_URL . 'css/' );
			define( 'CURRENT_THEME_ADMIN_FONTS_URL', CURRENT_THEME_ADMIN_ASSETS_URL . 'fonts/' );
			define( 'CURRENT_THEME_ADMIN_GIF_URL', CURRENT_THEME_ADMIN_ASSETS_URL . 'gif/' );
			define( 'CURRENT_THEME_ADMIN_ICO_URL', CURRENT_THEME_ADMIN_ASSETS_URL . 'ico/' );
			define( 'CURRENT_THEME_ADMIN_JPG_URL', CURRENT_THEME_ADMIN_ASSETS_URL . 'jpg/' );
			define( 'CURRENT_THEME_ADMIN_JS_URL', CURRENT_THEME_ADMIN_URL . 'lib/js/' );
			define( 'CURRENT_THEME_ADMIN_PNG_URL', CURRENT_THEME_ADMIN_ASSETS_URL . 'png/' );
		}
		
		if ( !sf_is_user_logged_in() ) {
			define( 'CURRENT_THEME_FRONT_URL', CURRENT_THEME_URL . 'front-end/' );
			define( 'CURRENT_THEME_FRONT_ASSETS_URL', CURRENT_THEME_FRONT_URL . 'assets/' );
			define( 'CURRENT_THEME_FRONT_CSS_URL', CURRENT_THEME_FRONT_ASSETS_URL . 'css/' );
			define( 'CURRENT_THEME_FRONT_FONTS_URL', CURRENT_THEME_FRONT_ASSETS_URL . 'fonts/' );
			define( 'CURRENT_THEME_FRONT_GIF_URL', CURRENT_THEME_FRONT_ASSETS_URL . 'gif/' );
			define( 'CURRENT_THEME_FRONT_ICO_URL', CURRENT_THEME_FRONT_ASSETS_URL . 'ico/' );
			define( 'CURRENT_THEME_FRONT_JPG_URL', CURRENT_THEME_FRONT_ASSETS_URL . 'jpg/' );
			define( 'CURRENT_THEME_FRONT_JS_URL', CURRENT_THEME_FRONT_URL . 'lib/js/' );
			define( 'CURRENT_THEME_FRONT_PNG_URL', CURRENT_THEME_FRONT_ASSETS_URL . 'png/' );
		}
		
		if ( sf_is_user_logged_in() ) {
			define( 'CURRENT_THEME_BACK_URL', CURRENT_THEME_URL . 'back-end/' );
			define( 'CURRENT_THEME_BACK_ASSETS_URL', CURRENT_THEME_BACK_URL . 'assets/' );
			define( 'CURRENT_THEME_BACK_CSS_URL', CURRENT_THEME_BACK_ASSETS_URL . 'css/' );
			define( 'CURRENT_THEME_BACK_FONTS_URL', CURRENT_THEME_BACK_ASSETS_URL . 'fonts/' );
			define( 'CURRENT_THEME_BACK_GIF_URL', CURRENT_THEME_BACK_ASSETS_URL . 'gif/' );
			define( 'CURRENT_THEME_BACK_ICO_URL', CURRENT_THEME_BACK_ASSETS_URL . 'ico/' );
			define( 'CURRENT_THEME_BACK_JPG_URL', CURRENT_THEME_BACK_ASSETS_URL . 'jpg/' );
			define( 'CURRENT_THEME_BACK_JS_URL', CURRENT_THEME_BACK_URL . 'lib/js/' );
			define( 'CURRENT_THEME_BACK_PNG_URL', CURRENT_THEME_BACK_ASSETS_URL . 'png/' );
		}
	}

	/*
	// Loads the Theme
	function load_theme() {
		// Globalize the $sf_theme_loaded varible
		global $sf_theme_loaded;
		// Set the $sf_theme_loaded variable
		$sf_theme_loaded = true;
	}
	
	// Loads the Plugins
	function load_plugins() {
		global $active_plugins;
		global $plugin_settings;
		global $installed_plugins;
		global $disabled_plugins;
		$sfdb = sfdb();
		$plugin_settings = $sfdb->plugin_settings();
		$installed_plugins = $plugin_settings->installed;
		$active_plugins = $plugin_settings->active;
		$disabled_plugins = $plugin_settings->disabled;
	}
	*/




	
	/**
	 * A timing safe equals comparison
	 *
	 * To prevent leaking length information, it is important
	 * that user input is always used as the second parameter.
	 *
	 * @param string $safe The internal (safe) value to be checked
	 * @param string $user The user submitted (unsafe) value
	 *
	 * @return boolean True if the two strings are identical.
	 */
	function timingSafeCompare($safe, $user) {
		// Prevent issues if string length is 0
		$safe .= chr(0);
		$user .= chr(0);
	
		$safeLen = strlen($safe);
		$userLen = strlen($user);
	
		// Set the result to the difference between the lengths
		$result = $safeLen - $userLen;
	
		// Note that we ALWAYS iterate over the user-supplied length
		// This is to prevent leaking length information
		for ($i = 0; $i < $userLen; $i++) {
			// Using % here is a trick to prevent notices
			// It's safe, since if the lengths are different
			// $result is already non-0
			$result |= (ord($safe[$i % $safeLen]) ^ ord($user[$i]));
		}
	
		// They are only identical strings if $result is exactly 0...
		return $result === 0;
	}
	
	// Checks if the user should login automatically and logs them in based on a token stored in cookie and database
	function auto_login() {
		//echo SITENAME;
		if ( isset( $_COOKIE[SITENAME.'_rememberme'] ) && !sf_user_logged_in() ) {
			list ( $uid, $token, $mac ) = explode( ':', $_COOKIE[SITENAME.'_rememberme'] );
			$_SESSION['logout_token'] = $token;
			
			if ( $mac !== hash_hmac( 'sha256', $uid . ':' . $token, SECRET_KEY ) ) {
				log_event('Suspected cookie spoof performed by '.$_SERVER['REMOTE_ADDR'] );
			}
			
			$sfdb = sfdb();
			$usertoken = $sfdb->get_login_token_by_id( $uid );
			
			if ( timingSafeCompare( $usertoken, $token ) ) {
				$_SESSION['logged_in'] = true;
				$_SESSION['uid'] = $uid;
				header('Location: https://'.NAKED_SITE_URL, true, 302);
				exit;
			} else {
				log_event('Suspected cookie spoof performed by '.$_SERVER['REMOTE_ADDR']." - They seem to have our algorythm. May just be an old cookie?" );
			}
		}
		if ( sf_is_user_logged_in() ) {
			if ( PROTOCOL != 'https' ) {
				header('Location: https://'.NAKED_SITE_URL, true, 302);
				exit;
			}
			
			if ( isset( $_COOKIE[SITENAME.'_rememberme'] ) && !isset( $_SESSION['logout_token'] ) ) {
				list ( $uid, $token, $mac ) = explode( ':', $_COOKIE[SITENAME.'_rememberme'] );
				$_SESSION['logout_token'] = $token;
			}
			
			if ( !isset( $_COOKIE[SITENAME.'_rememberme'] ) && !isset( $_COOKIE[SITENAME.'_uid'] ) ) {
				setcookie( SITENAME.'_uid', $_SESSION['uid'], NULL, '/', NAKED_SITE_URL, true, true );
			}
			
			$sfdb = sfdb();
			global $loggedin_user;
			$loggedin_user = $sfdb->get_user_object( $_SESSION['uid'] );
		}
	}
	
	function load_router() {
		require_once( CORE_SHARED_ROUTING . 'shared.router' );
	}

/***************************************************************************************************************************/
?>