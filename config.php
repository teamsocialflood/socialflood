<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	Project:	SocialFlood Social Networking Engine
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	File:		config.php
//	Path:		/
//	Version:	0.0.1
//	Updated:	3/28/2013
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Configuration Settings
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The type of database you are using. (mysql or pgsql)
	$db_type		= 'pgsql';
	// The database user you created.
	$db_user		= 'socialflood_8s2q';
	// The name of your database.
	$db_name		= 'socialflood_8s2q';
	// The password to your database.
	$db_pass		= 'o2!nd8k4Qd1Pt7';
	// The databade host address. (usually localhost)
	$db_host		= 'localhost';
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	If you would like to change the default database table prefix for obscuration then do so here.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$table_prefix = "s0cF"; // Set your desired table prefix here.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Error Reporting
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Setting true enables error control messages to be seen
	// Setting false hides error control messages
	$error_control = true;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	DO NOT EDIT BELOW THIS LINE!
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Define The Database Constants
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	define( "DB_TYPE", $db_type );
	define( "DB_USER", $db_user );
	define( "DB_NAME", $db_name );
	define( "DB_PASS", $db_pass );
	define( "DB_HOST", $db_host );
	define( 'DB_PORT', '5432' );
	define( 'DB_CHARSET', 'utf8' );
	define( 'DB_COLLATE', 'null' );
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Define The TABLE_PREFIX
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	define( "TP", $table_prefix );
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Define the Error Control & Reporting (ERR_CTRL) constant
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	define( "ERR_CTRL", $error_control );
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//	Error Control
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if ( ERR_CTRL ) {
		//error_reporting( 'E_ALL' );
		ini_set( 'display_errors', 1 );
		error_reporting( ~0 ^ E_NOTICE );
	} elseif ( !ERR_CTRL ) {
		error_reporting( 0 );
		//echo 'Error Reporting is turned off.<br>';
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
?>