// JavaScript Document	$(document).ready(function(e) {
	$(document).ready(function(e) {
        $('#expand_toolbox').click(function(e) {
			$('#user_toolbox_content').slideToggle(200);
			e.stopPropagation();
        });
		
		$('#user_toolbox_content').click(function(e) {
            $('#user_toolbox_content').hide(1);
        });
		
		$(document).click(function(e) {
            $('#user_toolbox_content').hide(1);
        });
		//alert("Document ready!");
    });
	
	$(window).scroll(function(){
		if  ($(window).scrollTop() == $(document).height() - $(window).height()){
			// run our call for pagination
		}
	});
	
	function set_avatar( url ) {
		BASE_URL = window.location.origin + "/";
		data = "api_token=<?php echo $new_api_token; ?>&set_avatar=true&url=" + url;
		
		$.ajax({
			url: BASE_URL + 'api/local/',
			data: data,
			dataType: 'json',
			type: 'post',
			success: function (j) {
				if (j.ok) {
					$('#set_avatar_link').removeClass("set-avatar-link");
					$('#set_avatar_link').addClass("set-avatar-done");
					$('#set_avatar_link').html("Used as Avatar");
				} else {
					alert("There was an error!" + j.msg);
				}
			}
		});
	}
	
	function done_uploading() {
		files = null;
		$('tbody.files').fadeOut(500);
	}